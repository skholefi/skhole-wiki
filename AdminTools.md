# Admin Tools

***

## Actions Tab

* Sync CRM
    * Manually sync licenses from Pipedrive CRM
    * During sync if organization has exceeding licenses - email notification of this are sent to org.admin and teachers requesting to archive users or buy more licenses, contains the link to users page with reached number of users.

* Sync Mailchimp
  * Sync lms users with mailchimp members.
  * Mailchimp list ids and corresponding queries to accounts collection in db are set up in config.
  * Currently we sync fi students, fi teachers, en students, en teachers.
  * Fake emails like test@test.com are not synced
  * Emails with not ascii symbols before @ are not synced
  * If user exists in lms, but he's missed in mailchimp, new member will be created in mailchimp.
  * If user exists in mailchimp, but he's missed in lms, obsolete mailchimp member will be archived.
  * If user exists in both lms and mailchimp, we will calculate sha1 hash of his data and if hashes are different, we will update him on mailchimp
  * When sync process is ended, admin can watch report with number of total, completed and errored sync operation on snackbar, moreover report archive is available for 10 minutes.
  * Mailchimp syncing also works daily at 4am and 6am by cron in background, if it's not synced alert to should be sent to #lms-messages.
  * If user is in multiple organizations, the last active account will be used

* Archive expired organizations
    * All expired organizations, groups and users will be archived

* Post organization statistics to Slack
  * Manually post organization statistics to Slack
  * By default, every 1st day of month cron job should post statistics to Slack in #lms-messages channel if _postStatisticsToSlack_ db flag set on for organization. Config looks as follows:
    _postStatisticsToSlack: {
      newUsersSignedUp: true,
      examCertificatesGenerated: true
    }_
    * Both properties are set to _true_. The following message will appear in the Slack channel "New users signed up and exam certificates generated on previous month: {mainGroupName} - January 2021 - {number} user signed up, {number} certificates generated"
    * Only _newUsersSignedUp: true_ is set. The following message will appear in the Slack channel "New users signed up on previous month: {mainGroupName} - January 2021 - {number} users signed up"
    * Only _examCertificatesGenerated: true_ is set. The following message will appear in the Slack channel "Exam certificates generated on previous month: {mainGroupName} - January 2021 - {number} certificates generated"

* Sync B2C Stripe

* Post monthly LMS statistics to Slack
  * manually trigger getting of the monthly extended statistics
  * Statistics once a month (the past month + cumulative from the beginning of current year if asked ):
    The whole Skhole:
  * Sheet 1: User statistics with columns:
    * Number of users who signed in (past month),
    * Number of users who signed in (current year cumulative)

  * Sheet 2: Course statistics with columns:  
    * Course
      * Include all Skhole's courses, also courses that were studied 0 times.
        * Completed past month (number), 
        * Started past month (number),
        * Completed current year cumulative (number),
        * Started current year cumulative (number),
        * Feedback, current year cumulative (number, average only),
        * Feedbacks, past month (text).

  * Sheet 3: Exam & quiz statistics (2 tabs: 1. Exams, 2. Quizzes) with columns:
    * Name of the Exam / quiz
    * Include only Skhole's exams and quizzes, not the ones made by other users
    * Completed past month (number), 
    * Completed current year cumulative (number),

  * By organization (including normal Skhole orgs AND Whitelabels):
    * Sheet 4: User statistics with columns:
      * Organization
      * How many licences in use (past month + cumulative year)
      * How many of those users signed in at least once (past month + cumulative year)
      * How many sign-in's in total (past month + cumulative year)

  * Sheet 5: Course statistics (Tabs for each organization) with columns:
    * Course
    * Include ONLY the courses used, no courses that were not studied
    * Include also organization's own courses
    * Completed past month (number), 
    * Started past month (number),
    * Completed current year cumulative (number),
    * Started current year cumulative (number),
    * Feedback, current year cumulative (number, average only),
    * Feedbacks, past month (text).

* Sync Visma Sign Certificates statuses
  * Updates all Certificates collection type certificates Sign status from Visma to Skhole

* Sync Course statistics with Sympa
  * Transfers Course statistics from Skhole to Sympa. The indentification data is the users email address (...@humana.fi). The data that should move is: Completed course (Courses name, duration (in hours or days) and completion date).
* System Messages
  * System message is sent to all users in selected organization(s).
  * It is sent as push notification if user has Email notifications -> App notifications on or otherwise by email.
  * If "Terms change" checkbox is checked then only users who agreed to receive notifications about changed Terms of use will get notified. Users can change notification preferences in Profile -> Settings.

* Merge users
  * Account to remove
    * action is irreversible
    * can not be groupleader account or admin
    * account will be removed, all account related data will be moved to the 'Account to leave' account
      * statistics
      * discussions/discussion messages
      * lesson reviews
      * certificates
      * owning of content
      * text/video notes
  * Account to leave
    * contains merged data of 'Account to remove' and 'Account to leave'

* Post course reviews
  * sends course reviews of own whitelabeled courses(previous month reviews) to courseReviewsSubscribers emails from whitelabeled options
  * content of the file
    * Organization
    * Group
    * User name
    * Course name
    * Rate (number)
    * Feedback (written)
    * Estimated skill level before course
    * Estimated skill level after course


### Statistics

Filters:
 * Date range
 * Organization target:
   * Choose by name
   * License count (provide min and max values)

1) Get CHURN

Get CHURN rate for target organization(s) on specified date range. Compare second newest and newest deal ranges.
If there are multiple target organizations average would be shown.


2) Get amount of licences assigned by pipedrive

Get max number of licences assigned for selected period.


3) Get amount of ending licences

Gets the number of licences going to expire from ending deals.

## Whitelabeling Tab
* Organization
  * (react-select)  
  * organization is already whitelabeled - editing of whitelabeling options will be shown
  * organization is already a child of whitelabeled organization - prohibited to edit or make such org to be whitelabeled
  * usual organization - allowed to make such org to be whitelabeled and add whitelabeling options
* Is Organization Whitelabeled
  * checkbox that makes org whitelabeled(if org is already whitelabeled it's disabled to revert this action)
* Slug
  * texfield to add/edit slug of organization
* Whitelabeling options editor
  * (jsoneditor field)
  * avj validation of json
  * allows to change any whitelabeled option with validation
* Form Save button
