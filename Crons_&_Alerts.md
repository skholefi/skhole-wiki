# Crons
Every day app run some hidden from UI logic at defined time.
Crons are created because in some cases their logic is related only to time and cannot be triggered in other cases or their triggering makes high loading for API.  

## Intercom clean useless users - every week on Thursday

## Remove expired files

## Send notifications

## Pipedrive sync

## Archived expired organizations, groups, users

## Complete exams with code

## Send email about expired deals

## Update timestamps in demo group

## Mailchimp cron

## Saml cron

## RabbitMQ heathcheck

## Issue certificates for closed exam sessions

## Update courses progress for closed not finished quiz sessions

## Update exam results for closed not finished exam sessions

## Database dumping

## Send monthly organization statistics to users (checkbox in email preferences)

## Send task notification reminder

## Send extended statistics reports

## Sync Visma Sign Certificates statuses

## Sync Course statistics with Sympa

## Transcoder health check

## Send course reviews of whitelabel's courses to the emails from courseReviewsSubscribers wb option

# Alerts
Crons might not be run 100% and alerts are a kind of triggers notifying the team about such cases. 

## App is down or slow (uptime bot)

## Background tasks are processing slow or not processing within last 15 minutes (logdna)

## Mailchimp has not been synced last day (logdna)

## Pipedrive CRM has not been synced within last day (logdna)

## [environment] Transcoder job has not been successfully completed for 30 mins

