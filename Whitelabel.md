# Skhole whitelabel #

Skhole can be configured for organization’s own colors, logo and fonts. Besides customization, whitelabeled organisation’s admin can create new “sub-organizations” that have their own organization admins, teachers and students. 

All sub-organizations are also themed with root-whitelabel.


Whitelabelled organization has their login page where they logout and logout. Only Organization’s users can login via that page.


Whitelabelled organization sees only their courses for now. Sharing of Skhole courses and vice-versa functionality will be built in autumn 2020.

Skhole’s technical support configures the whitelabel via database. Following options can be customized:

* Service name (multi language)
* Service slug (eg. https://app.skhole.fi/service-slug redirects to https://app.skhole.fi/{organizationLanguageCode}/service-slug)
* Service description (multi language)
* Service logo - desktop
* Service logo - mobile
* Service logo - email footer
* Service logo - social media
* Service favicon -icon
* Service login page title
* Font family (300,400,600,700, certificate fields)
* Font colors (certificate fields)
* Font sizes (certificate fields)
* Certificate templates (by default similar than Skhole, but black and white without logo)
* Organization’s own website name
* Organization’s own website URL
* Organization’s Facebook page URL
* Customer service email-address (default cs@skhole.fi) but if customer wants to change that its possible. * * Then need to configure also DNS records in Mailgun.
* Default sub-organization type
* Can organization admins assign editor access (true/false)
* Footer HTML - Additional HTML inserted into footer
* Video default loader image
* Colors
    * primaryColor
    * primaryDarkColor
    * primaryLightColor
    * secondaryColor
    * secondaryLightColor
    * toolbarWrapperColor
    * fieldErrorBackground
    * fieldDisabledBackground
    * fieldDisabledColor
    * socialBtnColor
    * mainProgressBarColor
    * newBadgeColor
    * editCourseHeaderColor
    * mainProgressBarColor
    * intercomActionColor
    * intercomBackgroundColor
    * dashBoardBackground
    * panelColor
    * courseHeaderBackground
    * courseHeaderOpacity (%)
    * tableRowContrastColor (on mobile)
* The link to organization's Terms & Conditions (termsUrl)
* General help url (generalHelpUrl)
* Hide Classroom from navigation (hideClassroom)
* buyButtonLabel - text on RestrictedAccessModal
* accessToSkholeCourses - flag that indicates that org has access to public courses(All normal Skhole organizations - access setting)
  (Whenever new course gets published or updated, if there is in access groups "All normal Skhole organizations" selected,  a query will run which makes sure that this courses accessGroups includes all organizations that have this accessToSkholeCourses flag)
* generalIntegrationPersonId - is used for users that don't need to export exam statistics
* integrationPersonIdVerificationEnabled - turns on espery integration person id
* teacherSeesOnlyOwnGroup - teachers can only see the student information of the same group that the teacher is in(users listing, statistics, exams, anything related to the users).
* hideAddGroupBtnForTeachers - teachers are not able to create new groups and don't see create new group button
* onlyExamsWithCode - exams can be created only with code, there is no other options for this
* mandatoryGroupInvitation - if true, teachers and higher roles should be able to invite users only in the group, logic is spreadable to all child orgs
* allowServiceNameFilter - Flag that defines visibility of "{serviceName}'s courses" filter on the home pagef
* discussionsSubscribers - Array which should contain valid emails, used for sending to these emails discussions messages/discussions itself created in wb own courses
Whitelabel organization users can use same functionalities as normal organization.
* courseReviewsSubscribers - Array which should contain valid emails, used for sending to these emails course reviews which have been added in wb own courses
* bookUserTrainingHelpVisible - flag which defines book the training option for whitelabeled users in Help menu(?) in header
* professionInputLabel - if not empty - used as a string label on users profession(register and profile)
* userSkillEvaluationEnabled - turn off the review and self-evaluation modals
* mainHeaderColor - Separate the header color from variable primaryColor.

# Skhole whitelabel with medical license #

Configuration: _isMedicalLicenseOrganization_ db flag set to _true_ under _whitelabelingOptions_.

Users have limited access to app features. The following navigation items are hidden from teachers: Users, Groups & Registration codes, Classroom, Quiz modules & Quizzes.

The ability to assign certificates is available only for exams. No certificate from courses or assignments.
