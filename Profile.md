Profile can be accessed from Header via clicking Profile-icon and choosing Profile

***

## Edit Profile

### Buttons

Upload from files
* User can click button to choose picture from his files or use can also drag file from his computer here. Image can then be cropped to square format in a modal window that opens.

Role.

Registration date.

Membership id for organization with enabled membership id verification.

Join organization
* User will be asked the code to join another organization. He will need to confirm joining via email confirm link. 
* After confirmation user will be able to choose organization in organization workspaces or after login.
* Each organization workspace is separated one from each other. Technically user has few accounts with the same email and can switch between them.
* He will get tasks assigned for his new groups in new organization workspace. 
* All statistics, notifications, public profiles, User role, Groups, products, expire date, will be separate for each organization workspace.
* Disabled admin role Skhole

View public profile.

When a user has empty profession, education level, gender and birth date we redirect him to profile page to fill these fields. 


### User can update fields
* First name [required]
  * _text input_
* Last name [required]
  * _text input_
* Password [required]
  * filling 2 password type fields "New Password" and "Confirm new password"
  * Passwords must use all four available character types: lowercase letters, uppercase letters, numbers, and symbols
  * must have at least 8 chars
* Email [required][unique per organization]
  * _text input_
  * _validates that no-one else has this same email in use in current organization_
  * if user is in multiple organizations, only admin can directly edit email, but all other roles will be forced to go through logic:
    1. User receives email confirmation
    2. User confirms the change from the link in the email
    3. Only after that user email will be changed.
  * if user has not confirm request to change email he will see notice about this
* Profession [required]
  * _text input_
* Education level [required]
  * _select box_
    * Vocational School
    * University of Applied Sciences
    * College degree
    * University
    * Other
* Gender [required]
  * _select box_
    * Male
    * Female
    * Other
* Year of Birth [required]
  * _select box_
    * years 1900-present...
    * default scrollTo year to 1985
* Bio
  * _textarea_
* PersonId - in case of user is in organization where integrationPersonIdVerificationEnabled whitelabeled option is true,
personId should be valid and exists in the last file in s3 bucket or it can be generalIntegrationPersonId.

***

## Certificates

User will get certificate from course completions and exams that have setting Skhole certified quiz turned on. This page will act as listing page for certificates.
On the left side there will be certification icon and certificate name. On right side a checkbox "Show in profile" and print icon.

### _Clicking certificate name_
* Opens up the certificate
  * Buttons
    * Print
      * Opens certificate for printing mode
      * For a trial user "Buy Skhole" modal is shown.
    * Download
      * Download as PDF file
      * For a trial user "Buy Skhole" modal is shown.
   * Regenerate the certificate (admin-only)
      * Regenerate image and pdf.
  * View of actual certificate image
    * This is to certify that {firstname} {lastname} has successfully completed {course/exam} {coursename/examname} on {date}
    * Date and certificate holder's signature (only for exams created based on quizzes with isSkholeCertified=true)
    * Certificate no: {6CHARS}
    * Certificate url: {siteurl}/cert/{6CHARS}
  * Actual certificate image are generated in background to keep fast response times for api. So when the image is not generated yet, user will see "Certificate image will be generated in short while". Once it's generated, image should appear on the opened page without its reloading.  
  * Trial user will see big "Trial" words over certificate image
  * Add Visma sign [only for Certificate collections][visible only for teachers and higher roles] [only for organizations that has turned on feature of Cert collections and digital signature]
    * Opens Add signer email modal - signer receives email with invitation to sign Certificate in Visma(https://sign.visma.net/api/docs/v1/)
      * Signer email 
        * Text-field
      * Submit button
      * Confirmation modal
    * After invitation to sign certificate teacher will see notice on cert page about who was invited to sign and when
    * Certificates sign status is synced daily
    * For signed certificates - Certificates contains 2 pages - 1 is Certificate image with sign watermark, 2 - sign details.
    * Signer has 90 days to sign certificate after that invitation will be cancelled and will be possible to invite someone else again to sign certificate
  * Show details button [only for Certificate collections]
    * Modal with list of certificates included to the Certificate collection Certificates, links are clickable links to certificates
  * Remove certificate button [only for Certificate collections]
    * action is irreversible 
    * Shows confirmation modal of certificate removing
    * removes certificate and related certificate collection
      * when there is pending status of Visma sign invitation - cancel invitations and remove document in Visma
      * when certificate has been signed in Visma - removes certificate in Visma.
      

### Show in profile
Adds link to the certificate in users public profile.

### Print
Opens certificate for printing mode.

***

## Settings

### 2-Factor Authentication
* enable 
  * button
* required for admins
* link to FAQ

#### Turn on 2FA modal
* Instruction section
* QR code and info for manual setup
* Code that required from Google authenticator app
  * Text field
  * [required]
* Save button

#### Turn off 2FA/Confirm action that requires 2FA modal
* Verification code
  * textfield
  * [required]
* Choose another verification method
  * Modal
    * Option 1 - Google authenticator
    * Option 2 - Email confirmation
* Resend code button
  * available only when verification method is "Email"
* Save button

### Email preferences
* App notifications [checked by default for all users] - all in-app notifications will be duplicated to user's email address
  * Organization monthly statistics [checkbox, by default false]
    * Is available only for teachers, groupleaders and groupmanagers. If true - once a month user will receive email with next statistics.
      * Course statistics
      * Quiz statistics
      * Assignment statistics
      * Ended exams
    * Links in emails are clickable. If user is not logged in when clicked the link, he should log in to be able to get the file, authorization dialog should appear on link click and when successfully logged in he would receive the file.

  * Include only my groups 
    * Teachers will receive only statistics from their groups

### I don't want to give Skhole access to my data
By choosing "Stop using Skhole" we will delete all the data

"Stop using Skhole" (button)

_GDPR compliancy feature, where user can stop using Skhole. When user stops using Skhole, all of his data is hidden from others in statistics, user listing, public profile etc. User can come back later on to this page and click on "Start using Skhole" to start using Skhole again._

### User profile(viewed by other users)
* User name
* Role
* bio
* Completed courses
* Public certificates 
* Certificate collection certificates
* Documents list(not the public info)
* Edit Profile(for users who have access to this)
* Create certificate collection
* Create document(if allowed by user role and organization)

#### Create a summary certificate modal
Modal for creation Summary certificate certificates. It's certificates that includes other certificates and can be digitally signed.
* Certificate name
* Choose certificates
  * [react-select]
  * public certificates(except other Certificate summary certificates)
* Attach up to 3 PDFs files to certificate
  * add pdf files button
  * remove pdf file button
  * preview of pdf files
* Create Certificate summary

#### Create a document modal
Modal for creation Documents based on forms. Document can optionally include certificates and can be digitally signed.
* Choose form
  * [react-select]
  * published forms list
  * allowed to select only one form
* Select button
Second step of the process of creation documents is filling in forms fields based on selected form
* Static fields from form displayed as a text(Image, Form title, Subtitles, Textarea)
* Fields from form that are required to fill in by teacher:
  * Checkbox 
    * _Label of the checkbox is used from form, teacher can select/unselect/leave empty checkbox_
    * _In case if checkbox is not selected - it will be not displayed on document at all_
  * Subheading
    * _Textfield that is required to be filled in by teacher, label and position is defined in form_
  * Fill in blank
    * _Textfield, label is defined in form, on document will be displayed as LABEL_FROM_FORM: TEACHER_INPUT_
  * Date
    * _Date picker, label is defined in form, on document will be displayed as LABEL_FROM_FORM: DATE_
* Choose certificates
  * [react-select]
  * public certificates
* Attach up to 3 PDFs files to certificate
  * add pdf files button
  * remove pdf file button
  * preview of pdf files
* Create Document summary