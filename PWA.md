# Removed on Release 9.1.23(https://app.asana.com/0/1203573520838753/1203108375989311) [leaved here in case of some bugs]
PWA stands for Progressive Web Application. We at Skhole use PWA functionalities for Offline courses, so this article will focus on that.

***

## Offline courses


### Course downloading

Text version of lesson contents and lesson notes will be downloaded into users browser cache.

User can click download course -icon on

* Index page's course listing when hovering any of the courses
* On Course details -page

Tooltip for action is

*"Download for offline"*

When user clicks download icon, snackbar-message:

*Downloading course "{courseName}", related lessons and notes...*
When download is finished, snackbar-message:
*"{courseName}" offline access download complete*

User can download only one course at time. Tooltip for action when course is already downloading is ""

*It's possible to download only one course at time. Please try again later.*


### Offline state

If user has downloaded courses into his browser, doesn't have internet and navigates to

* https://app.skhole.fi{/ or not} + {any-Of-the-Languages}/{/ or not}

an index page with offline courses pops up. There will normal Skhole header, but without any links because no actions besides courses can be taken in offline state.

There is displayed under header:

*You don't appear to have internet connectivity so you can only view downloaded content*

User can click on course and go study text lesson format and see his notes. Mark complete button is available, but notes taking, reset course progress and video commenting are disabled.


### Offline state to online syncing

When user with offline content downloaded appears back online he will get brief snackbar notification "Syncing offline state...".

At that time, offline state's lesson progress will be synced back to online version.

If there is an update required for PWA, users will be notified of this with possibility to proceed or dimiss update.


### Removing course from offline state

Users who have downloaded some course, can click remove downloaded course -icon on online-version:

* Index page's course listing when hovering any of the courses
* On Course details -page

If removable course is last of their offline cached courses, then user is prompted with modal with information:

*Removing this course from offline cache temporarily disables offline mode. All pages will reload*

when approving that, offline cache will be removed, PWA will be disabled for user and app pages reloaded. This mechanism is built because it's not good to keep PWA activated for users who don't require offline sync (majority of users). It is confusing for them.