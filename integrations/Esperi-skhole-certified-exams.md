# Esperi integration to export Skhole certified exams statistics
This integration is designed for getting Completed Skhole-certified exams for users that are in Esperi organization
and have integrationPersonId. Integration is available through 2 routes:
* apiUrl/api/quizzes/skhole-certified-completed-exams?page=1&perPage=50
* apiUrl/quizzes/skhole-certified-completed-exams/:personId?sort=value&dateTo=YYYY-MM-DD&dateFrom=YYYY-MM-DD

* New "person id" field is added to Skhole for use of this organization specifically. User will need to provide this field in sign up. Can be changed in profile. All users must have it, otherwise integration wont work.  
* To customers app, list of passed medical license (Skhole certified) are obtained for users (all and individually). User are fetched based on the person id field.
* Skhole API added IP-address filter for use of this organization. Ip addresses is provided via mainGroup options and shouldn't require api restart.
* List of valid PersonIds is gotten daily by cron from AWS S3 bucket, all users that are not in updated list and previously have correct PersonId will be not able to use Skhole until provide valid PersonId. User needs to provide a correct person id for service to work. One person id for account.
  * Skhole side provides aws bucket and credentials for Esperi, bucket name is esperi-integration.
* generalIntegrationPersonId - id that allows to users use Skhole, but it's common for all users, in case of usage this personId, statistics will not be exported.
  * now integration is turned on for Esperi organization and generalIntegrationPersonId is 377374ID

All details are provided in Google doc: https://docs.google.com/document/d/15bLdvZepIVIjkF93nz5rhXSmKNH8c99A/edit?usp=sharing&ouid=102061148633520635929&rtpof=true&sd=true