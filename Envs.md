|  |  Production |  Staging | Test |
|----------|-------------|------| --- |
| UI url |  https://app.skhole.fi | https://staging.skhole.fi | https://test.skhole.fi |
| API url |  https://app-api.skhole.fi | https://staging-api.skhole.fi | https://test-api.skhole.fi |
| Basic http auth |  - |  admin@skhole-lms-2018 |  admin@skhole-lms-2018
| Purpose | Stable version | Pending release + E2E testing | One task ready for testing
| AWS bucket |  skhole-app |  skhole-app-staging |  skhole-app-staging
