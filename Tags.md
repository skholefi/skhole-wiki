Admin can add/edit/archive tags via Group menu->Tags.
Tags are used for example in "Browse by category" filter next to search in Header, also as filters when searching for lessons in course's add lessons modal.

***

**Header functionalities**

* Search for tag
    * text search, table automatically refreshes on typing
* Active/Archived 
    * (radio buttons)
    * filters by tag status
* Archive selected tags (confirm archiving modal)
    * Action button
    * If tag is archived - it's automatically removed from all courses where it attached
* Add new tag
    * Opens add/edit tag modal
* Filter by language
  * React-select field, populated from database. This is different from UI language and filter content by content language field.
  * By default equals UI language
  * Includes option 'All' - user can see tags with any language

**Table listing**
### Logic
* Display all Skhole tags as disabled for editing on the tags list page for WB organizations. If the category of WB organization is non-healthcare - do not display Skhole tags at all, only WB's tags.

### Table components and columns
* checkbox
    * to choose tag(s), then perform actions on it (currently only action Archive selected tags exists)
* Name
    * String
    * Name of tag
    * can click Header name of this field to toggle sort by this field asc/desc
* Created
    * Tag create date
* Edit
    * Opens edit modal for row's tag

**Add/Edit modal**
* Tag name [required] [unique]
    * String
* Language
  * [React-select]
  * allowed one option