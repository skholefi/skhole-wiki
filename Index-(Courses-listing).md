This page describes logic and UI of index-page /

***

# Not logged in

User will see [Header](https://github.com/elifTech/skhole-ui/wiki/Header) for not logged in user + listing of courses visible to not-logged in users.

***

***

# Trial
## Trial is ended
User will see that page with message that his trial period was ended and instructions how to buy Skhole
***

# Not supported browser is detected
If user use not supported browser or browser that platform considers as outdated, user sees static page with suggestion to change browser.

# Logged in

User will see [Header](https://github.com/elifTech/skhole-ui/wiki/Header) and [Dashboard](https://github.com/elifTech/skhole-ui/wiki/Dashboard) for logged in user + listing of courses visible to him/his organization. Courses with open tasks will be shown in the begining. Courses marked as featured always will be shown secondary.


## Filters

### Browse by content category
* Content categories is not included the search. Choosing any category will auto-refresh current search. Choosing multiple categories will result to search showing courses where some of the content categories is used. 
* If organization field of expertise is Non Health Care, this filter is hidden
* By default, contains pre-selected content categories that match with user organization field of expertise(the same field in db)

### Browse by classification
* Classification(tag) filters are not included in the search. Choosing any tag will auto-refresh current search. Choosing multiple tags will result to search showing courses where some of the tags is used.
* If there is no courses by category for current user, we don't show this category
* When language filter is changed - auto reset of tags and updated list of possible tags to select based on language

### Content type
* Course
* Podcast
* Live broadcast

### Language
* React-select field, populated from database. This is different from UI language and filter content by content language field.
* By default equals UI language
* Includes option 'All' - user can see content with any language

### Filters for Courses (only for authorized user)
* All Courses
* Skhole Courses
    * _(courses created by admin@skhole.com)_
* Other courses
    * visible only for admin
    * _(courses created by not admin users)
* My courses
    * Courses where user has open task
    * Started courses (except reset ones)
    * Favorite courses
    * Courses created by the current user
* {serviceName}'s courses
    * visible only for whitelabeled organizations with _allowServiceNameFilter: true_ wb option
    * _(shows courses created by wb organization editors from which user is)_

...and in select box order_

* Publish date
* Recently accessed (only for authorized user)
* Progress (however if asc, then completed courses will be shown at the end)  (only for authorized user)
* Name
* Popularity (number of completed lessons/quizzes of course)

Order

* Descending (default)
* Ascending

Reset filters button

Grid/List switch - state is saved for user

State saving

* All filters save their state in url meaning page refresh, link sharing or back navigation in browser history should lead to the home page with already selected filters and matched results.

* "Clear search" button is visible for both logged in and not logged in users when filters are updated.


### Single courses

Besides displaying number of lessons and duration (for course type), "favourite heart icon" that user can click to toggle course from favourites (only for authorized user).

User's progress in course is being indicated with blue color on bottom of each course card. - only for authorized user

"Download for offline / Remove from offline" icon-button to download text content of the course for offline access for a month. - only for authorized user

***

# Course listing

32 courses will be displayed initially, with more button on bottom that loads 32 more courses into listing.

Single course card contains number of lessons and duration is being displayed, for example "11 lessons (3h 8min)". - only for course type of course.

Each course has badge
* "New" - created less than a month ago
* "Featured" - has featured flag
* "Task" - there is active task to pass the course 
* "Course" - simple course type of course in other cases
* For broadcast type
  * "Live" if current date is before broadcast end date
  * "Recording" if current date is after broadcast end date
  
Course duration can be hidden in case no medias were uploaded.

Hours are omitted in case it's zero.

For broadcast type of course:
 * Show start/end date in 23.10.2021 14:00 - 24.10.2021 14:00 format if current date is the same as/before start/end dates
 * Show 23.10.2021/23.10.2021 - 24.10.2021 format in all other cases

Short description of course is shown if list view mode is selected.

After each click on course item in this list GA collects special event course_click, that saves also additional info about course, user and organization.
This allows to get statistics and report directly from Google analytics(Explore -> Course_click -> export to spreadsheet). The retention period of this data is 1 year.

Load more button is shown if there are more items to load.

# Lesson listing
* If user uses search field, we show relevant lessons under courses list also.
* Lesson block looks similar to course block (preview, name, duration)
* Preview is the default image
* In list mode, lessons description is shown.
* Separate load more button
