# Sympa courses statistics export integration

## General info
Integration is designed for exporting course statistics to Sympa HR system. Only completed course statics is exported.

Export is done daily by cron job at **05.30 a.m** or can be triggered manually from admin tools.

The interface of exported statistics:

```{
    '@context': '#$delta',
    value: [
        {
            Email: 'user email',
            'Courses@delta': [
                {
                    Name: String // 'course name',
                    Organizer: String  // is always 'Skhole',
                    StartDate: String // 'learning start date of the course in format ('YYYY-MM-DD'),
                    EndDate: String // 'completion date of the course in format ('YYYY-MM-DD'),
                    Duration: Number // decimal number - duration of the course in hours, e.g. - 3.4,
                    Planned: String // is always 'YES',
                    Paid: String // is always 'YES',
                    Skhole_courseid_unique: Int32 // unique int32 field that is generated in Skhole and used as idempotency key,
                }
            ]
        }
    ]
}
```

* For turning on integration for any organization "**sympaPartner: true**" should be added to organization document in '**maingroups**' collection.
  * The logic is spreadable to the child whitelabeled organizations, so if integration is turned on for root WB organization then statistics will be exported from root and each child organizations.
* For each course statistics record of users from organizations with turned on integration, during creation of record, sympaUniqId value is generated(unique int32 number)  and saved by path _integrations.sympa.sympaUniqId_ in _usercourses_ collection
* Statistics is exported only for users, which are returned from Sympa https://api.sympahr.net/api/SkholeEmployees get employee route.
* After successful export of statistics to Sympa on the level of mainGroup the 'sympaSuccessSyncDate' is saved and on the level of '_usercourses_' collection _integrations.sympa.syncDate_ is saved as well.
  * structure of the usercourse sympa related export record is: 
  ```  integrations: {
      sympa: {
        syncDate: Date,
        sympaUniqId: { // has unique sparse index, so can but null/undefined but should be uniq if presented
          type: Number,
        },
      } 
  ```
* Daily completed course statistics starting from mainGroup 'sympaSuccessSyncDate' date is taken and sent to Sympa, all items with '_usercourses_' collection _integrations.sympa.syncDate_ value are ignored.

## Error handling
Due to Sympa documentation and statement from Sympa - if Sympa API returns error, then any statistics item from payload is not saved in Sympa system.

Based on it, the Skhole API handles  errors in the next way:
* If Sympa API returns any kind of error
  * Skhole considers that any record has been not saved in Sympa so during next sync current and new stat items will be included in payload.
  * Skhole saves errored Sympa response and payload to the collection 'sympasyncresults'
  * Skhole API sends to the _customerservice_ Slack channel notification about error.
  * Next sync will include old payload as well.

Additional information:
* Sympa API information - https://drive.google.com/file/d/1dSuOPu3uB1J4wHQfXLeTDWKqnz2iCD4n/view?usp=sharing
* Sympa/SKhole/Humana specification - https://drive.google.com/file/d/1jKBUzfXw3o1GXx8tSUbWhkY4SUcHDstK/view?usp=sharing

Sympa contact tech person - Matti Vehnämäki <matti.vehnamaki@sympa.com>