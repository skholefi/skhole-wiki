# Quiz execution

***

## UI

### Header

On top there will be name of the Quiz (or Exam) as h2 and arrow (pointing left) on the left side. Clicking the arrow will result to back functionality.

### Content part

* General information of quiz
    * Time limit
        * _h:min:s such as 00:45:00_
    * Attempts
        * (display only if retries specified in quiz or exam settings)
        * _for example Attempts: 2/3_
    * Due date
        * (only for exam, coming from task settings)
            * _for example Due date: 2019-03-12 11:12_
* Number of questions as clickable cubes
     * _user can navigate between questions by clicking "cubes"_
* Start Exam (or Quiz) -button 
     * _Clicking the button starts timer and quiz*
     * If exam pass % is 0, then report will be marked as completed once its started. Also if exam has "Assign certificate" or it's based on Skhole certificated quiz, certificate will be issued.
* Quiz description
* Supervisor name(Only for Skhole-certified exams)[required if exists]
* Supervisor organization(Only for Skhole-certified exams)[required if exists]

### User personality confirmation modal
Before the start of the Skhole-certified exam users should confirm their identity.
* Components:
  * Title with user's first and last name
  * Confirm button
    * after clicking to it user can start the exam
  * No it's not me button
    * after clicking to it users will be signed out.

## Ongoing Quiz

Quiz mode should stick to portrait mode. If user turns device to landscape mode a warning modal will pop up and ask to turn device to portrait mode. User can choose to ignore it and continue quiz.
Cubes have black, blue, green, red or yellow border in case

**Quiz**

* Green
    * _User has answered to question correctly_
* Red
    * _User has answered to question incorrectly_

**Exam**
  
* Black
  * _User has answered to question_

**Quiz & Exam**

* Grey
  * _User has not answered to question yet_

* Blue
  * _Current question_

* Yellow
    * _User has marked question for review (pressed Review button)_

* Timer will refresh every second
* Review question -button
    * _results to question cube being marked as under review (yellow color)_
* Summary -button
    * _results to summary view being displayed. Basically summary view just hides the current question. Also it has Submit-button that user can use to submit quiz early._
* Question info
    * _for example "Question 5 of 20"_
* Question
    * _Question itself_
    * _Answer options and choices depending on question type_
* Next button
    * _Proceed to next question_
* Finish button
    * _Next button changes to finish button when in last question. Clicking that results to quiz ending and Results page being displayed._

## Quiz/Exam sessions

* Quiz/Exam session can be restored (but not paused) if task deadline or time allocated for quiz attempt are not passed.

* There are 3 triggers for closing the session:
    * Press finish button
    * Session has been auto submitted (user is on page) when allocated time is expired.
    * By cron every 5 minutes when none of two previous triggers fired.
* When trigger is fired then, attempt should become visible in app: exam results, quiz/course statistics, certificate should be issued.
* Attempt is not visible until its session is finished/closed.  

***

## Results

X of XX questions answered correctly

Your time: h:min:s

You have reached X of XX point(s), (X%)

* Restart button
    * _Will be labelled "Restart exam (X/XX)" depending how many attempts user has. If user does not have anymore attempts, this button will not be displayed._*
* View Quiz Report
    * _If allowed in quiz settings, user can view more detailed report of the quiz such as how he answered and what were the correct answers._

***

## Report

On the report there is info on top

* Name of the user who attempted
* Points in total
    * for example 14/30 (46.67%)
* Pass requirement xx%
    * (student passed | student did not pass)
* Number of attempts
    * (number)
* Status
    * not started - exam end date is in the past and user did not attempt the exam
    * finished - user completed exam or exam end date is simply in the past
    * show unlock button - when it's neither "not started" nor "finished" and there is no available attempts anymore. Unlocking means adding 1 extra try
    * open - user still have free attempts for exam and none of previous cases match
* Progress
    * ?
* Go to result -button
    * _Go back to previous page_

**Questions**

Note that order displayed here is not the same that users had.

If question points are set to 0 it is not marked as incorrect.

(in the end)

there are again Go to result button and "Restart quiz" button which lefts user restart quiz if he has attempts left.
Only for admins:
* Alter to max score, passed and assign certificate
* Alter to not passed and revoke certificate

***

## Pointing

Quiz will be scored as specified in quiz's questions settings. However there is extra logic regarding multiple choice questions. For all incorrect answers, user will get minus points as much as that answer would have given points. However, user cannot get negative points from questions for example if there are 4 choices, user answers them all wrong, he will get 0 points from question.

Another example, if there are 6 choices in the question of which 3 are correct and all have points specified. User answers 2 correctly, he will get 4/6 points from the question. He got 2 minus points as 2 is the maximum points assigned to choices in that question and that was reduced once from the total of 6 points.

***

## Certificates issuing

If exam task is based on "Skhole certified quiz" and user passed it, he will get a certificate that can be also found on his profile page. 

Also if task has "Assign certificate" flag, certificate will be issued. In case task creator passes exam, he will be asked for first name last name and organization.

***
