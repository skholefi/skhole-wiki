After successful login, user will see Dashboard section before Courses listing. By default Dashboard is opened.

User can toggle dashboard visibility from its bottom right corner "Hide dashboard"/"Show dashboard". 

Selection will stick throughout the browsing session, but on each login Dashboard state will reset to being open by default again.

Dashaboard is not rendered on server, just on ui to improve performance.

Trial users can not see discussions in dashboard listing
***

### Continue learning
* User sees card of last course studied and can click that to route himself to continue from where he left off.
* Block is omitted if user has only one available course in the list of courses

### (Information about tasks)
* You have xx tasks to do
    * clicking will route to tasks
* You last scored xx/xx at {examName} exam
    * clicking will route to tasks
* You last scored xx/xx at {examName} quiz
    * clicking will route to that quiz
* You have x new message(s)
    * clicking will toggle open notifications

### Discussions
* Show latest discussions topics from Classroom or Course discussions
