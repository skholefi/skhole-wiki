Same discussion functionality is used in public course discussions and organization's internal Classrooms.

Topic's creator will automatically follow discussions and get [notifications](https://github.com/elifTech/skhole-ui/wiki/Notifications) of new replies

Discussions and Classroom are disabled for trial users.

***

### Discussions header
* Choose organization (admin-only)
* Search for discussion
    * Search from the posts, (name, content, replies)
* Post an entry
    *Opens a new modal to create a post
        * Subject
            * (text input)
        * Message
            * (DraftJS field with basic formatting options)
 
### Discussions listing
* Topic author's avatar
* Discussion
    * Topic
        * if it's a video timeline note, the topic will be "{lessonName} ({positionMinutes}:{positionSeconds})"
    * Truncated content of topic's message
    * Link-icon, if its a course related discussion
    * Lesson-icon, if its a lesson video timeline note
    * Timestamp of topic and authors name
* Latest
    * Timestamp of latest response
* Responses
    * Amount of responses in topic
(on empty table show the default "No content here yet" image)
* Paging

### Topic (actual message)
* Back arrow-left routing back to discussion listing
* Topic name -header
* Paging
* Item row
    * Avatar
    * Message author name
    * Timestamp of message
    * Follow topic -icon (click to toggle following)
    * Actual message
    * Remove icon (visible if admin / +also if groupleader/teacher/groupmanager in classroom)

***

## Classroom

Classroom discussions are visible only inside the organizations. All organizations have their own classrooms.
Notifications about new discussions and discussions messages are sent to the specified Slack channel.
Classroom has 2 tabs

* Group discussion
    * Listing of discussions
* Followed discussion
    * Listing of followed discussions

***

## Email notification about new discussions/discussion-messages to discussionsSubscribers(list of emails in whitelabeled options)
Each discussion/discussion-message are sent to emails from  discussionsSubscribers list in whitelabeled options

* applied only for discussions/discussion-messages in own courses of whitelabeled organization
* emails are sent in the course language
* logic is spreadable to the child wt organizations
* discussionsSubscribers - array of emails that can be even not Skhole users, if array is empty - only usual logic is applied
