Admin or users with Editor privilege can add/edit/archive authors via Group menu->Authors.
Authors can be added to Lessons, then displayed in Course's page.

All authors are related to language: en, fi or sv.
API server returns authors in language selected on UI.

***

**Admin**
* can select organization which's authors to edit
* see/edit/archive all organization's authors

***

**Users with Editor privilege can**
* see/edit/archive authors from their own organization's courses

***

**Header functionalities**

* Select organization
  * (for admin only)
* Search for author
  * text search, table automatically refreshes on typing
* Active/Archived
  * (radio buttons)
  * filters by author status
* Archive selected authors (confirm archiving modal)
  * Action button
* Add new author
  * Opens add/edit author modal

**Table listing**

* checkbox
  * to choose author(s), then perform actions on it (currently only action Archive selected authors exists)
* thumbnail
  * uploaded image of author or default pic
* Name
  * String
  * Full name of author
  * can click Header name of this field to toggle sort by this field asc/desc
* Profession
  * String
* Created
  * Author create date
* Edit
  * Opens edit modal for row's author

**Add/Edit modal**
* Image field (250x250)
  * uploaded image of author or "Drag and drop author image..." -text
* Author name [required]
  * String
  * Full name of author
* Profession [required]
  * String