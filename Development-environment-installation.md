## API
* `nvm use 12`
* `yarn`
* `docker build -t skhole-cli -f Dockerfile.transcoder .`
* if docker has issues building it, disable transcoding container in docker-compose.yml comment out section
`transcoder:`
    `image: skhole-cli`
    `links:`
      `- rabbitmq`
      `- db`
* `docker build -t skhole-api -f Dockerfile .`
* `NODE_ENV=staging docker-compose up`

## UI
* `nvm use 12`
* `yarn`
* `yarn start`

## Run app with local db

Preconditions:

* Mongo 4.2 client (https://docs.mongodb.com/manual/tutorial/install-mongodb-on-ubuntu/ there are instructions for MacOS also). Be sure you are installing only mongo client, you don't need to have mongo server on you host machine because it will bind to the same port and another mongo server in docker container won't be able to start. If somewhy you have mongo server on you host machine, stop it and disable it's autorun to allow another mongo server in docker container to bind the same port.

* docker-compose up (to have running mongo server in container)

* Download latest dump from skhole-dumps bucket, be sure date of dump is right, because i noticed their long app with sorting in scope of one page))), so you need to go to another page and scroll down to find the latest dump.
https://s3.console.aws.amazon.com/s3/buckets/skhole-app-dumps/?region=eu-central-1

* Uncompress downloaded dump file

* Rename dir "skhole-production" to "dump" open terminal in current dir (not "dump")

* Paste mongorestore --db skhole --drop --gzip
After this app should work normally, however "docker-compose up" - stop and start might be required if database did not exist before
