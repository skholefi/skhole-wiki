# B2C organizations

## Definitions

Sairaanhoitajat has whitelabeledOptions.membershipIdVerificationEnabled.

Free suborganization reference is in whitelabeledOptions.freeMembershipOrganizationId.

Paid suborganization reference is in whitelabeledOptions.paidMembershipOrganizationId.

https://sairaanhoitajat.skhole.fi - prod checkout ui

https://staging-stripe.skhole.fi - staging checkout ui

## General

B2C organizations do not use general flow with sign up codes for registration, users are asked for unique membership id instead.

Membership id is unique in scope of whitelabeled organization.

User can see their membership id on profile page.

## Flows

### Free membership flow

When sign up modal form is opened for Sairaanhoitajat organization, user should paste valid unique not used membership id.

After registration user will be in whitelabeledOptions.freeMembershipOrganizationId and he is active as long as memebership id is valid.

### Paid membership flow

When checkout ui is opened, user should select billing period (month or year), provide his name, last name, email and valid unique not used membership id.

After form submission user is redirected to stripe checkout page, where they need to provide payment credentials. Check stripe test credentials for test env. Use coupon on production env for testing.

After checkout form submission, user will receive invite email to his inbox. Email contains link, when user follows it, they see registration form with some prefilled data(it will not expire after 14 days)

After registration user will be in whitelabeledOptions.paidMembershipOrganizationId and he is active till end of billing period as long as memebership id is valid.
At the end of billing period, there is membership id checking, if it's invalid, user will be notified and archived. Otherwise user will be billed for the next billing period. 

On profile page user has a possibility to check his subscription plan and cancel it at the end of the billing period. Stripe admin can cancel it right now on stripe dashboard to return money.

### Other users flow

For registration in other sub organizations, users invite flow can be used.

If user membership id is not valid anymore, then user's subscription is canceled at the end of billing period and email notification "Your subscription is expiring" is sent.

## Where do we take membership ids?
There is sftp server where Sairaanhoitajat uploads csv file with list of actual ids.
For staging there is another server.

Every day we connect to this sftp server and pull list of valid ids. If some existing free user's membership id is removed from the list, it will be archived next day.
