On this section it's possible to edit single questions that can be integrated in quizzes.

***
### Search
User can search items by question or answers values

### Filters
Possible to filter by [All Questions, Questions from Skhole, Questions from other users, My Questions] (select), related course (select), related lesson (select), text filter on question name.
* Filter by language
  * React-select field, populated from database. This is different from UI language and filter content by content language field.
  * By default equals UI language
  * Includes option 'All' - user can see questions with any language
  * 
### Table rows
Question name on left, Type column and action buttons on right. Action buttons are

* Copy
    * Duplicate a question
* Edit
    * Modify existing question -> Open question modal
* Remove
    * Remove question if you are allowed to, with confirm dialog

Table has pagination.

### Ordering
Tasks table is ordered by default createDate DESC. Clicking on the Name and Type -columns headers will toggle sorting by that column DESC<->ASC.

### Creating a question
* Click on the + icon on top right of the table header, question modal opens.

### Creating a question
* Click on the + icon on top right of the table header, question modal opens.

***

# Question Modal

Save question by pressing bottom left Save icon button on the modal or discard from top right.

## Fields

* Question type
    * (required, select)
* Name
    * (required, text input field)
* Private
    * (required, select)
* Points
    * (number input field 0-100, no decimals, 1 by default)
    * zero is allowed, we show warning instead of error for this case  
* Question
    * (required, DraftJS + images support)
* Tags
    * (react-select)
* Associated Courses
    * (react-select)
* Associated Lessons
    * (react-select)
* Quiz modules
  * (react-select)
  * quizzes/exams in which question is used, editing of this field will remove/add(in the end) question
* Language
  * [React-select]
  * allowed one option

## Question types / Answers

options for specific types of questions

### Multiple choice
Choose from multiple answers, there may be multiple correct.

Possible to add multiple answers. "Add new answer" on the bottom.

Should be at least one correct answer.

Answer options

* correct
    * (checkbox)
    * _if checked, that answer is a correct answer. Can choose multiple._
* Description
    * (long text input field, max 1000 characters)
    * _the answer itself_
* (Remove) Icon
    * _Remove that answer_

### Single choice
Just like Multiple choice above, but can choose only single correct answer.

### True or false
This questions answer is either true or false.
Can choose True or False from radio button. Required to choose either.

### reflection exercise
This questions answer is free of score(gives 0 points, but always correct), user can fill in answer to the text field with 2000 chars limit.
 * Note
   * text field 
   * answer or other note from teacher(NOT MANDATORY) that the user making the quiz could see after completion of the quiz in the quiz report

### Fill in blank
Quiz taker needs to input some of the answer choices.
Possible to add multiple answers. "Add new answer" on the bottom.

#### Case-sensitive
Checked by default.

When it's checked, "ANSWER" in question options will be not be equal "answer" from student.

Answer options

* Description
    * (long text input field, max 1000 characters)
    * _the answer itself_
* (Remove) Icon
    * _Remove that answer_

Statement/Answer options

* Description
    * (long text input field, max 1000 characters)
    * _the answer itself_
* (Remove) Icon
    * _Remove that answer_

#### Basic math
Dynamically creating calculation questions allows us to create huge amount of questions "at once". 

Formula and variables in that formula should be added. In Question draft.js field variables will be used in order to show question for users. 

Question

* {varName} - will be replaced by generated value for varName

Allowed decimals(answer) *
* Select with values that specify what number of decimals is required after the comma
    * No preference - to the answer will be specified basic math rules _1 = 1,00_
    * Not allowed - any decimal value should be rounded by math rules to the integer
    * [1, 2, 3] - appropriate number of decimals are required

Formula
* Text field where user must add formula for calculations. It can include eager static number value either variables.
All text values will be evaluated as variables and each variable will need to be added in variables section.
Allowed math operations are [+, -, /, *]. During passing exams/quizzes users will not see formula, 
but answers will be checked by this formula with substituted values instead of variables.
Users will see only _Question_ where editor can use variables which will be substituted with generated values.

Variables(editor can add multiple variables)
* Name
* Min/Static value (if max value is not specified than min will be used as a static value)
* Max value
* Range step - value of step between min and max values. Variable will be generated in range from _Min value_ to _Max value_ with _Range step_

Generate test question
* Question that end user will see
* Your answer - text field(use to test setup of the question and expected answer result)
* Check the answer - button to see check answer to test question


###### Example of basic math question:

Question(draft.js field):   
``
How much is {var1} - {var2}
``  
Formula: ``var1 - var2``  

Variables: ``var1=4; var2=3``  
Allowed decimals: ``2 decimals`` 

Generated question:  
_How much is 4 - 3_

Correct answer: ``1,00``   
Answers with values _1 or 1,0_ will be wrong (because of allowed decimals field)




