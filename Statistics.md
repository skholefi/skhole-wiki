## Course statistics

Filters(when going to some Certificate via course or quiz statistics, enable filters to stick, saved in query params)

* Course
    * react-select
* Organization
    * (visible if admin)
    * react-select
* Group
    * react-select
    * (need to choose organization first if admin)
* User name
    * react-select
* Date range
    * opens modal window where user can select from calendar from and to dates, then click Apply
    * date range should be less than 18 months for all users except admin
    * if single user is selected, then there is possibility to select date from user's registration date
    * on back-end we assume that student started or completed course in selected date range
* Completed only
    * Filters only courses that have been completed
* Print -icon
    * Opens view as printer friendly + automatic print opens 
* Export Excel -icon
    * Starts preparing Excel export for download. User is notified when excel is ready to download.
    * Also users' subgroups will be included into xlsx file

### Course listing
(all table headers can be clicked to toggle order)

* Avatar (or icon with name initials)
* Name
    * Clicking name routes to public profile
* Course name
  * On hover - course time is displayed(duration)
* Start date
* Completed parts - button
    * button label
      * xx/xx for course type of course
      * hh:mm:ss for broadcast type of course.
        * refreshed every 5 minutes
        * (playing) is shown if user watched broadcast recently
        * multiple watching sessions are summed up. there is known case with summing time up when user opens several tabs
  
    * Clicking this button opens modal "{courseName} results of {lastName firstName}" with listing of all completed lessons
        * Lesson name(click opens lesson)
        * Completion time
            * can be clicked to toggle order
        * Paging
* Completion time
    * (table ordered default by this field desc)
    * Certificate icon link button should be available
* Paging

***

## Quiz statistics

Filters

* Quiz
    * react-select
* Organization
    * (visible if admin)
    * react-select
* Group
    * react-select
    * (need to choose organization first if admin)
* User name
    * react-select
* Date range
    * opens modal window where user can select from calendar from and to dates, then click Apply
    * date range should be less than 18 months for all users except admin
    * if single user is selected, then there is possibility to select date from user's registration date
* Passed only
    * Filters only quizzes that have been passed (pass % achieved)
* Print -icon
    * Opens view as printer friendly + automatic print opens
* Export Excel -icon
    * Starts preparing Excel export for download. User is notified when excel is ready to download. 
    * Also users' subgroups will be included into xlsx file
    * Link with exported file is available only for owner

### User+quiz listing
(all table headers can be clicked to toggle order)

* Avatar (or icon with name initials)
* Name
    * Clicking name routes to public profile
* Quiz name
* Number of attempts -button
    * Clicking this button opens modal "{quizName} results of {lastName firstName}" with listing of all attempts of chosen user+quiz sorted by completion time desc
        * Result %
        * Time spent (XX:XX:XX)
        * Completion time
            * can be clicked to toggle order
        * Correct answers (xx/xx)
        * Pass/Not pass icon
        * View report button if available
            * Opens [Quiz Report](https://github.com/elifTech/skhole-ui/wiki/Quiz-execution-&-Report)
            *  if user lost access to quiz button will be disabled and "Report cannot be opened because you don't have access to the quiz" will be shown
        * Paging
* Average
* Last attempt
    * (table ordered default by this field desc)
    * Certificate icon link button should be available when exam is passed
* Paging

***

## Group Top performers

* Choose time range
    * user can select from calendar from and to dates
* Lessons studied
    * Name of student
    * Number of lessons studied in date range
    * Order
        * (select)
        * Descending (default)
        * Ascending
* Quiz improves 
    * Name of student
    * Name of quiz
    * Last score
    * Percentage of improvement over time
    * Order
        * (select)
        * Descending (default)
        * Ascending
    * See https://docs.google.com/spreadsheets/d/1bpINY9qsPCIM7rUnRiTKEebRLhfajCWe5cRiHDQdChU/edit#gid=0 for more details

***

## Demonstration statistics

Filters(when going to some Certificate via demonstration statistics, enable filters to stick, saved in query params)

* Organization
    * (visible if admin)
    * react-select
* Group
    * react-select
    * (need to choose organization first if admin)
* User name
    * react-select
* Date range
    * opens modal window where user can select from calendar from and to dates, then click Apply
    * date range should be less than 18 months for all users except admin
    * if single user is selected, then there is possibility to select date from user's registration date
    * on back-end we assume that student started or completed demonstration in selected date range
* Completed only
    * Filters only demonstrations that have been completed
***

## Users activity (admin only)

Idea of this section is to give admin a better understanding of licenses adoption for selected customer segments.

* Select type of organizations
    * All
    * School
    * Profession
* Organization name
    * react-select
* Choose time range
    * user can select from calendar from and to dates

Chart

Y = number of users

X = Dates

Hover over the dates to see information between this date and the previous one:

* active users
* licenses count
