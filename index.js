const glob = require('glob');
const fs = require('fs');
const path = require('path');
const handlebars = require('handlebars');
const async = require('async');
const httpStatus = require('http-status');
const express = require('express');
const showdown = require('showdown');
const basicAuth = require('basic-auth-connect');


const APIError = require('./helpers/APIError');

const app = express();
const converter = new showdown.Converter({ ghCompatibleHeaderId: true });
converter.setFlavor('github');

const templeteFile = fs.readFileSync('index.html', 'utf8');

const getPages = cb => {
  return glob('*.md', {}, (err, files) => {
    if (err) {
      return cb(err);
    }

    const pages = files.map(file => ({
      link: path.parse(file).name,
      name: path.parse(file).name,
    }));

    return cb(null, pages);
  });
};

const getContent = ({ pages = [], content = '' }) => {
  const template = handlebars.compile(templeteFile);
  return template({ pages, content });
};

if (
  process.env.BASIC_HTTP_AUTH_PASSWORD
) {
  app.use(basicAuth('admin', process.env.BASIC_HTTP_AUTH_PASSWORD));
}

app.get('/', (req, res, next) => {
  getPages((err, pages) => {
    if (err) {
      return next(err);
    }

    const content = getContent({ pages, content: '' });

    res.header('content-type', 'text/html');
    res.status(200);
    res.send(content);
  });
});

app.get('/:file', (req, res, next) => {
  const { file } = req.params;
  return async.auto({
    pages: getPages,
    content: cb => fs.exists(`${file}.md`, (exists) => {
      if (!exists) {
        return cb();
      }

      fs.readFile(`${file}.md`, (err, mdContent) => {
        if (err) {
          return cb(err);
        }

        return cb(null, converter.makeHtml(mdContent.toString()));
      })
    }),
  }, (err, { pages, content }) => {
    if (err) {
      return next(err);
    }

    res.header('content-type', 'text/html');
    res.status(200);
    res.send(getContent({ pages, content }));
  })
});

app.use('/static', express.static('static'));

app.use((err, req, res, next) => {
  if (!(err instanceof APIError)) {
    const apiError = new APIError(err.message, err.status, err.isPublic);
    return next(apiError);
  }
  return next(err);
});

app.use((req, res, next) => {
  const err = new APIError('API not found', httpStatus.NOT_FOUND);
  return next(err);
});


app.listen(process.env.PORT || 80, () => {
  console.log('Server is running...');
});
