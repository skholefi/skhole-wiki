# Visma digital sign for bundle-certificates

Integration is designed for adding digital sign to the bundle-certificates(certificates collection certificate).
Core logic:
* When organization have turned on digital sign feature, teachers and higher roles have possibility to request some person to sign bundle certificate.
* For this they should add signer email, and request to sign certificate will be sent to signer.
* After request to sign, certificate gets 'pending sign' status, and it's impossible to cancel or change this status.
* If certificate is not signed during 90 days, it becomes cancelled, and it's possible to add sign again.
* Appearing singed certificate in Skhole is not immediate process, daily cron job syncs signed certificates(or it can be triggered manually in admin tools).
* After successful sync of signed certificates, certificate is displayed as signed with all information. 

Visma API documentation - https://sign.visma.net/api/docs/v1/

### Some additional information from task specification:
1. Create the Certificate collection modal
   Groupleader/teacher/orgmanager creates a certificate collection with few certificates, also user can attach up to 3 pdf files with limit 10 MB, that will be included into certificate, there is 1 action button in certificate-collection create modal:
- Generate bundle-certificate (After clicking that, the collection vanishes and turns into a bundle-certificate (pdf)) (see video 1)

2. bundle-certificate should contain next information:
- name of the certificate
- date
- organization (plus groups if mentioned in original certificate)
- supervisor/teacher
- certificate no.
  see picture 1 with a mockup

2. We have a "Visma sign" button on bundle-certificate. (bundle-certificate is visible for student and  teacher/admin/manager see Visma sign button)

3. When Groupleader clicks on it he sees the modal "Add email of the person that will receive a request to sign certificate" Here should be a notification that it is very important to write the correct email address.

4. Groupleader adds email and submits the form(there is email templates that Visma sign service will automatically sent https://sign.visma.net/api/docs/v1/?_ga=2.20525944.1195215798.1673523275-1495413847.1673523275#action-create-invitations-with-templates )

5. Until the (signer) of the certificate does not sign it,  bundle-certificate will have pending sign status ( show in status)

6. When the bundle-certificate is signed by the doctor, the groupleader that sent it to be signed should get an email notification about signing

7. Signed bundle-certificate page(not the certificate image) should contain information about who signed it and the date of signing, a link to the signed pdf document, and a list of certificates links that are included in the bundle-certificate

8. Sync between Visma documents and Skhole website should be done daily
9. Admin in admin tools should be able to sync statuses at any time by clicking on the button 'Sync Visma documents'

## Info for testing and credentials to test account

Username: 33504johanna.honkela@skhole.fi
Password: 4af31a27-10fc-44af-9b9f-ff410de6acca
during signing as a auth method choose "Test identification" url should be identification.frakt.io/