## LogDNA

LogDNA (the product) is a cloud services based server log management and analysis tools that helps developers pinpoint production issues by aggregating all system and application logs into one platform.

Currently logs are enabled for both staging and production.
All logs have message and optionally meta data e. g. user, organization etc facilitating searching.

### Api logs
We use `logdna` npm package integrated with `winston`. We use winston.{info|error} is used instead of console.{log|erorr}.

Also `express-winston` logs all requests to api and log message meta might look like:
* req
* * url
* * method
* * httpVersion
* * originalUrl
* * reqId
* query
* headers
* res
* * responseTime
* * statusCode
* * reqId
* user
* * _id
* * email
* * mainGroup

Api project has also daily crons with crm syncing, archiving expired groups etc, all their messages can be found on logdna also.

### Browser logs
For UI we use the same `logdna` npm package, but it's brownfield by this instruction https://github.com/logdna/nodejs/issues/55
Domains where logdna is working in browser should be added to whitelist https://github.com/logdna/nodejs/issues/52

All console.error will be logged.
Also we log browser routing e.g.:
* isBrowser
* user,
* req.url
* query
* locale
* responseTime
* res.statusCode

### SSR logs

UI project is running not only in browser env, but also on node express server, it prerenders pages before user will show them in browser. 
We use `express-winston` also for it, log meta data should look similar to api data.  

### Trancoder logs

Also we have separate script for transcoding lesson media files running inside docker-container on UpCloud.
We use simple `winston` package for logging there.

## AWS S3 bucket with logs

Currently we use Birch plan on LogDNA, so we have only 7 days of logs retention.

To be able to access logs before we save compressed json with them every day to 
https://s3.console.aws.amazon.com/s3/buckets/skhole-logs/?region=eu-west-1&tab=overview

## Other logs

Logdna does not log Heroku router logs (for API and SSR), they can be accessed via Heroku cli.
However we can see only 1500 latest messages with it.
Also all logs appear here faster than on LogDNA
