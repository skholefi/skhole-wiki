Lessons are at the core of Skhole's offerings. Most lessons are in all 3 formats - video, audio and text.

***

## Default view

By default when clicking a lesson open, video player on full screen starts to play.

Clicking player results to fullscreen been closed but video still playing. Now there is also the text content visible on right.

On Iphone video autoplay is disabled.

If user did not interact with page before (e. g. lesson page reload), video player won't autoplay because it's against browsers policy.

## User interface

### Header
Lesson header on top
Header consists of
* Arrow-left
    * (icon)
    * _Takes back to course page if lesson taken in scope of course. If just single lesson, then link will take back to index (/).

* Lesson name
    * Page header-text (h2)
* Leave course rating button
    * shows only when user completed at least 1 lesson/quiz of the course
* 3 format icons
    * (round icon buttons)
    * _Possible choice combinations: [video, video+text, text, audio+text]_
* (Question mark, ? icon)
    * General help
        * _(link to Skhole's customer service site)_
    * Current page help
        * Hidden in single lesson mode (when lesson is accessed directly from search out of course scope)
        * _Show current page's Tours_
* Navigation icon
    * Hidden in single lesson mode (when lesson is accessed directly from search out of course scope)
    * (hamburger icon, only visible if taken lesson in scope of course)
    * _Clicking it toggles visibility of course hierarchy navigation floating on right_
        * Lesson Format
        * (icon, choices [lesson, quiz])
    * Name of lesson
    * Duration of lesson
    * Completion status
        * (icon)

### Content area

Video player on left or full width depending on choices, text on right or full width depending on choices, audio player on bottom right.

### Sources (when text mode is enabled)

Lesson can have optional text field with sources displayed right after main content.

### Attachments (when text mode is enabled)

ZIP/DOC/DOCX/XLS/PDF files can be attached to the lesson.

Each attachment has name, upload date&time, format (+icon), size and download button. 

### Action buttons

* Hidden in single lesson mode (when lesson is accessed directly from search out of course scope)

Below content on bottom left.

* Add note
    * _Visible only to logged in users_
    * _Visible only when video player is visible (because requires video timeline)_ 
* Notes
    * _Visible only to logged in users_
    * _Enabled if there are some video or text notes_
    * _Toggles video player note overlays / text notes highlighting_
* Complete
    * _Visible only to logged in users_
    * Mark lesson manually as complete
* Show certificate (only in scope of course and if course is completed)
    * Link to page with certificate saying current user completed course
* Go to {Next lesson name}
    * Link to the next lesson when current is completed 

### Automatic actions

* Fires only in course scope when user is logged in
* Playing at video timeline over 90%
* Playing at audio timeline over 90%
* Scrolling text content over 90%

all above  ^ => Marks the current lesson complete

## Video player

Based on VideoJs (https://videojs.com/).

User can control the video player via icons: play/pause, sound volume, video timeline (progress bar), change speed [0.7, 0.85, 1 (=default), 1,15, 1.5], toggle fullscreen, format settings [1080p, 720p, 360].

Video might be also loaded from YouTube/Vimeo. Vimeo and Youtube type of video does not allow to add notes in timeline and in general uses native Vimeo/Youtube player different from our.

Videos are autostarted on desktop if user interacted with current page before while on mobile we do not have video autoplay.

Player with transcoded videos should not have blackborders because its sizes are defined based on original video sizes, its aspect area and space available for a player.  
## Text notes

Can be added by highlighting text content, then "Highlight" button becomes visible near that selection. 

Clicking that results to text been saved as "note". Users can highlight multiple parts of text to add additional notes. Highlighted areas can be re-highlighted to remove note. Highlighting is indicated w/ background color yellow.

Notes are disabled on mobile and tablet.

## Video notes

When video player is visible, it's possible to add video notes. Clicking the button "Add note" results to small modal opening on bottom left corner with fields

* Leave your comment at xx:xx
    * (textarea)
* Private
    * (checkbox, enabled by default)
    * disabled for trial users
    * If not private Send note to #customerservice
* Action buttons
    * Close
        * Close Add note modal
    * Save
        * Save note

Video notes are being displayed for 8 seconds and indicated on timeline as markers.
Read more on those here,
http://sampingchuang.com/videojs-markers


## Print mode
When in any lesson, choose "Print" from browser menu.

User can print clear page with only text content and sources in it.
Same print should be obtained no matter if user is in video, text or audio format.


## Lesson Review modal
Only logged-in users may leave a review for lesson content whether it's interesting, too difficult etc.

When lesson or podcast lesson completed there appear "Completed! Rate content" button. Clicking that "Rate content modal", opens modal where user can review the content.
 * User can close it without leaving rating.
 * Opening Lesson Review modal stops the automatic proceeding to next lesson.
 * Opening Lesson Review modal when lesson video/audio is playing stops it and renew playback on modal close.
 * If user already reviewed a lesson, he could change his rating.
