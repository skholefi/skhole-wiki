Welcome to the skhole-ui wiki!

This wiki is deployed to https://skhole-ui-wiki.herokuapp.com/ (admin - skhole-lms-2019).

It can deployed via `git push heroku master` (https://dashboard.heroku.com/apps/skhole-ui-wiki/deploy/heroku-git)


## Other resources

Skhole LMS mindmap by Tuomas
[![Skhole LMS mindmap by Tuomas](https://drive.google.com/uc?export=view&id=18nqcBusrJ1FCRV2THJfvEULioPBgHZM4)](https://drive.google.com/file/d/14i510fC6sXm9LhuBgk6SavWlAiVoSub-/view?usp=sharing)