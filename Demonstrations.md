Demonstrations can be found from Group menu in header, navigate to Demonstrations.

Demonstrations are used in Tasks as Demonstration task.

All roles except student can access Demonstrations section which can be found under Group menu in header.
Group admins and teachers can

* create new demonstrations
* edit their own demonstrations
* view/duplicate demonstrations.
* archive/restore demonstrations
***

# Demonstrations (section)

### Filters
Possible to filter by Organizations(for WB groupleaders and groupmanagers or admins), text filter on demonstration name, active/archived.
* Filter by language
    * React-select field, populated from database. This is different from UI language and filter content by content language field.
    * By default equals UI language
    * Includes option 'All' - user can see quizzes with any language

### Table rows
Demonstration name on left and action buttons on right. Action buttons are

* Copy
    * Duplicate a demonstration
* Edit
    * Modify existing demonstration -> Open demonstration modal
* Archive/Restore
    * Archive/Restore demonstration if you are allowed to, with confirm dialog

Table has pagination.

### Ordering
Tasks table is ordered by default demonstration name DESC. Clicking on the Name -columns header will toggle sorting by that column DESC<->ASC.

### Creating a demonstration
* Click on the Create demonstration button on top right of the table header, demonstration modal opens.

***

# Demonstration Modal

Save demonstration by pressing bottom left Save icon button on the modal or discard from top right.

**Fields**

* Name
    * (required, text input field)
* Guide
    * (long text input field, max 1200 characters)
    * _The aim of this field is to provide guide for student/teacher about goal or instructions of current demonstration_
* Number of acts
    * (required, select box, default 5)
    * range options from 1 to 9
* Demonstration is ready, publish.
    * (checkbox, checked by default)
    * _If not checked, users are not allowed to copy and use demonstration in the task_
* Results text
    * (long text input field, max 800 characters)
    * _Show this text on demonstration certificates_
* Language
    * [React-select]
    * allowed one option
 