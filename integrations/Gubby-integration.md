# Gubby integration

Gubby is the client which use some api routes directly bypassing interface.
It uses some teacher/groupleader/groupmanager cred in order to authenticate requests to Skhole API.

Gubby uses next routes:
* accounts/invite-by-code - bulk invitation of users
* accounts/invite - usual invitation of users

Doc shared to Gubby - https://docs.google.com/document/d/1RPazPa71kHdc_nWN2nN-AAHQFBlGtSy4oAbbC3_gLP0/edit?usp=sharing

Gubby contact tech person - Bianca Behm bianca.behm@gubbe.io

