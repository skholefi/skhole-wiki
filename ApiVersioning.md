In order to maintain backward compatibility, the Skhole API utilizes API versioning. The API version is included in the request URL, following the pattern https://app-api.skhole.fi/api/v{version}/route-name, where 'version' is an integer value (e.g., 1, 2, etc...).

## Version is not deprecated

* Response contains X-API-Version header, which defines API version by which request has been handled.
* The X-API-Version header should be used to ensure compatibility. The API URL includes the version number.

## Version is about to be deprecated

* Headers: X-API-Deprecation-Warning, X-API-Deprecation-Date
* Before the deprecation of an API version, the headers X-API-Deprecation-Warning(contains some info about deprecation) and X-API-Deprecation-Date(date) will be added.
* Those headers should be tracked for advance notice of deprecation.

## Version is deprecated

* Headers: Deprecation, X-API-Version.
* Server responds with 503 and a JSON message indicating deprecation. It suggests using the latest version.