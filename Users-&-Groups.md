As roles: Organization admin, teacher, organization manager and admin.
Navigating to Group Menu -> Users opens a Users section which works as described in this article

There are 3 tabs on this page for teacher/groupleader/groupmanager:
- Users
- Group Settings
- Organization info

There are 2 tabs on this page for admin:
- Users
- Organizations Settings
 
All datatables have pagination.

***

# Users

In this section, teachers and organization admins can manage users of their own organization. Teachers cannot modify organization admins but can modify other teachers. Organization admins can modify teachers. Admins can manage everything. Admin is allowed to edit archived users.

## **Table Settings**

## Fields and rows

* (checkbox) + "All" (on top row)
    * _Choose all rows of all pages in table_

* (checkbox)
    * _Choose that row(user) and perform action on it_

* (avatar/icon)
  * _If user has uploaded avatar, it will be displayed here. If not, then round icon with first letter of lastname and firstname, for example DJ = Doe, John._

* Name
    * _Users name as "Lastname Firstname", on second row users email address on smaller font_
    * _Groups where user belongs into_
    * Person id(if integrationPersonIdVerificationEnabled wb option is true)

* Last activity date
    * _Timestamp when user was last seen in system._

* Account expires
    * _Last day when user will still be active in system_

* Type
    * _User's role [admin, organization admin, teacher, student, organization manager]_

* Editor
    * _User has editor privilege_

* (Action buttons)
    * _"Groups"(only for admin in organization settings) => Load page section as "Group Settings"_
    * _"Edit" button -> opens "edit user modal"_

## Special fields and rows when pending filter selected
* Invite sent(instead of Account expires)
  * date of invitation

* Created By
  * name of the user who invited new user or self-join mark.


## Ordering
Tasks table is ordered by users last name ASC.
Clicking on any of the columns headers except Group(s) will toggle sorting by that column ASC<->DESC.

## Filters
Possible to filter table by Organization (admin only), group (need to choose organization first, there is option [Not in any group]) search for user (text search, search from email/firstname/lastname/firstname lastname/lastname firstname, esperiIntegrationId(complete match )), User activity status -active/archived.

## Actions
Next to filters there is Action selectbox with Run button next to it. Checkbox on users row(s) need to be checked to perform actions on selected users.

Actions doable via select box are

* Archive 
    * Archive user(s) instantly. Archived users will not be able to use Skhole.
* Set Expiration date
    * Modal "Setting expiration date for the following users" opens where expiration date needs to be defined. Users will be archived when this day specified day ends. Modal shows all selected users in a scrollable list. Possible choices on bottom right of the modal, "Oops! no thanks"(cancel) or "Submit"(enabled when expire date selected)
* Send reset password email*
    * Email containing link to change password is sent to user(s) email address

## Edit user modal

Save changes to user by pressing bottom left Save icon button on the modal or discard from top right.

*Fields*

* First name [required]

* Last name [required]

* Email [required][unique per organization]
  * if user is in multiple organizations, only admin can directly edit email, but all other roles will be forced to go through logic:
    1. User receives email confirmation
    2. User confirms the change from the link in the email
    3. Only after that user email will be changed.
  * if user has not confirmed request to change email he will see notice about this

* Remove change email request
  * Checkbox - visible only for admins during editing multi organization users if such request exists

* Editor
    * (checkbox)
    * _if checked user has editor privilege_

* Teacher
    * (checkbox)
    * _if checked user has teacher role_
  
* Form editor
    * (checkbox)
    * _if checked user can create/edit forms_

* Organization
    * visible only for admin, and wb groupleaders and managers
    * disabled during update for all roles except admin. Admins can change organization for existing users
* Groups
    * (react-select)

* Expire date
    * (choose day from calendar)

Modal has following action buttons below form:

* Switch to user
    * (visible for admin only)
* View public profile

***

# Group settings

In this section, teachers and organization admins can manage groups of their own organization.

## Filters
Possible to filter table by group name, activity status - active/archived.


## Fields and rows

* (checkbox) + "All" (on top row)
    * _Choose all rows of all pages in table_

* (checkbox)
    * _Choose that row(group) and perform action on it_

* Name
    * _Group's name_

* Active from
    * _Timetamp when group was created_

* Expiration date
    * _Last day when group will still be active in system_

* Student Sign up code
    * _Student signup code for group_

* Teacher sign up code
    * _Teacher signup code for group_

* Students
    * _Amount of students in group_

* Actions
    * Edit
        * (button)
        * _opens "edit group modal"_


## Edit group modal

*Fields*

* Group name
    * (required, text input field)
    * (must be unique)

* Student sign up code
    * (required to be unique and 6 chars from [WERTYUPASDFGHKZXCVNM2345679], text input field)
    * _has also a auto-generate button next to it on right side_

* Teacher sign up code
    * (required to be unique and 6 chars from [WERTYUPASDFGHKZXCVNM2345679], text input field)
    * _has also a auto-generate button next to it on right side_
    * once new user resisters with this code, user becomes a teacher in this group

* Student sign up code
    * (required to be unique and 6 chars from [WERTYUPASDFGHKZXCVNM2345679], text input field)
    * _has also a auto-generate button next to it on right side_

* Maximum students
    * Can be used to limit number of students in this group

* Archive organization on
    * _(choose day from calendar)_
    * _organization should be archived 23:59 on that day (so in the end of that day)_

* Teachers
    * (react-select)
    * Manage teachers of the group

# Organization Settings (admin-only)

Admins can manage also organizations.

## Filters
Possible to filter table by group name, activity status - active/archived.

## Actions
Next to filters there are action buttons
* Archive selected groups
    * _(archives groups on rows that have checkbox checked)_
    * if organization is whitelabeled, its child organizations also would be archived
* Add new group(/organization if admin)
    * _(open add/edit group modal)_

## Fields and rows

* (checkbox) + "All" (on top row)
    * _Choose all rows of all pages in table_

* (checkbox)
    * _Choose that row(group) and perform action on it_

* Name
    * _Organization name_

* Type
    * (for Organization settings/admin only)
    * _Choices: [School, Professional], displaying some strings (e. g. student -> employee, teacher -> supervisor) differently based on this selection_

* Licenses
    * _Number of licenses automatically fetched from Pipedrive CRM, can be changed manually only when Pipedrive sync is off_

* Active from
    * _Timetamp when group was created_

* Expiration date
    * _Last day when group will still be active in system_

* Pipedrive ID
    * (required when pipedrive sync is enabled)
    * _Pipedrive CRM organization ID from which deals into licenses are being synced with Skhole_

* Sync status
    * _ Icon showing state of Pipedrive CRM Sync - On/Off_
    * If sync is not disabled, admin can press on it to sync only deals of one organization, however "Sync CRM" button is required to be pressed if new products were assigned to organization.

* Student Sign up code
    * _Student signup code for organization_

* Teacher sign up code
    * _Teacher signup code for organization_

* Actions
    * Groups
        * _for managing groups of organization -> loads the current section as "group settings" instead of "organization settings"_
    * Edit
        * (button)
        * _opens "edit group modal"_

## Edit organization modal (admin-only and WB groupleaders and groupmanagers)

Save changes to user by pressing bottom left Save icon button on the modal or discard from top right.

*Fields*

* Organization name
    * (required, text input field)
* Pipedrive id
    * (required if Pipedrive Sync on, number input field)
    * (for Organization settings/admin only)
sync is off, otherwise field is disabled_
* TOL
    *(always disabled field, updated during Pipedrive sync, used in statistics reports) 
* Disable Pipedrive CRM syncing
    * (checkbox)
    * _manually manage licenses via licenses field_
* Organization admin
    * (for Organization settings/admin only)
    * (react-select, with possibility to enter a new email address)
    * _choose organization admin from existing (including archived) or provide a new email address_
    * existing or new user will get email (in selected lang) with organization access codes, login and password (only when new user) and other instructions 
    * existing user from different organization will become multiple organization user with the same password for each workspace
* Fields of expertise
  * (async react-select)
  * can be selected multiple options, if Non healthcare option is selected - then only this single option can be selected.
* Type
    * (radio)
    * Options: [School, Professional]
* Lang
    * (select)
    * Options: [fi-FI, en-EN, sv-SE]
* Allowed domains
  * (text-fields - if organization has at least one domain field, then new users will be able to sing up or join to organization only with email that is in allowedDomains list, this setting is applicable to all organizations not only whitelabel. Child whitelabel organization could have their own setting. If nothing provided then should follow root organization setting.)
* Licenses
    * (required if pipedrive sync disabled, number input field)
    * _Number of licenses automatically fetched from Pipedrive CRM, can be changed manually only when Pipedrive sync is off_
* Archive organization on
    * _(choose day from calendar)_
    * _organization should be archived 23:59 on that day (so in the end of that day)_
* Student sign up code
    * (required to be unique and 6 chars from [WERTYUPASDFGHKZXCVNM2345679], text input field)
    * _has also a auto-generate button next to it on right side_
* Teacher sign up code
    * (required to be unique and 6 chars from [WERTYUPASDFGHKZXCVNM2345679], text input field)
    * _has also a auto-generate button next to it on right side_
    * once new user resisters with this code, user becomes a teacher in this organization
* Certificate collections and digital signature
  * Checkbox

# Assign new organization admin modal (archived organizations only)
The modal pop ups when admin tries to update group leader's organization in "Edit user" modal. When user press "Confirm" temporary admin is automatically created and assigned as the group leader. If user press "Cancel" he will need to choose manually group leader from existing list.

# Organization info (teachers, group managers and organization admins only)

There are 4 areas on this page:
* Info
* Access codes
* Organization managers select (visible only for whitelabeled organizations, edit can only admin and groupleader, for other roles this field is disabled)
* Products with deals

## Info
* Organization name
* Creation date
* Number of active users
* Expiration date
  * whitelabled child organization admins do not see it

## Fields with access codes
* Student sign up code
* Teacher sign up code
* (optional) Access codes for addon products

## Organization managers select
Async select for Organization managers  (for whitelabel organizations).
Same scope of permissions than organization manager, but with the exception that it cannot modify organization manager or other managers. Organization manager must reside in root organization.

## Products with deals
* List of all available products (courses-packs) with {used}/{available} licenses is shown on the first column.
* Once product is selected, then list of all available courses is shown in the second block. Element "Skhole courses" is not clickable, while other courses are clickable.
  * Whitelabled child organization admins do not see deals column
* The third column shown list of all product deals with number of licenses and activity dates range sorted by their start date. If deal is active it has blue (primary column), if it's inactive (past or future) it has gray color.
* Last sync date of esperi integration ids (only for integrationPersonIdVerificationEnabled wb option is true)
