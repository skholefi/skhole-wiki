# Usage of Skhole API server via api keys
* The API Key must be used in order to authorize requests to Skhole API.

* In order to use Skhole API, Skhole should turn on this feature for the organization.
* API key can be created by Organization Admin from Skhole UI, on API Keys page and will share the same level of rights and access as Organization Admin for allowed routes

* Allowed routes are defined in the documentation https://staging-api.skhole.fi/v1/public-api-specification#section/Authentication and file on api server/constants/apiClientRoutes.js

* Skhole defines which routes are accessible for API keys, if some route is presented in this documentation, but you can not use it with your API key, then sooner Skhole prohibited usage of it for API keys of your organization

* For security reasons Skhole does not store API key in database, instead we store hash of API key, so in case of losing of the key, Skhole will be not able to restore it, and new key should be created

* In order to authorize requests to Skhole API X-Api-Key header must contain created API key.
