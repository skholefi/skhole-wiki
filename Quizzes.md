Quizzes can be found from Group menu in header, navigate to Quizzes. There are 2 tabs on this page, Quizzes and Questions.

Quizzes can be integrated into courses as part of the course hierarchy or used in Tasks as Exams.

Questions are separated as their "modules" so that they can be used in multiple quizzes.

All roles except student can access Quizzes section which can be found under Group menu in header.
Group admins and teachers can

* create new quizzes & questions
* edit their own quizzes & questions
* view/duplicate quizzes created by Skhole, with the exception of Skhole certified quiz
* use all quizzes as exams and define their own passing %, with the exception of Skhole certified quiz pass percent cannot be changed

Skhole certified quiz is a checkbox setting used in Quizzes. Skhole certified quizzes can only be viewed/modified by admin role.

***

# Quizzes (section)

### Filters
Possible to filter by [All Quizzes, Quizzes from Skhole, Quizzes from other users, My Quizzes]-selectbox, related course, text filter on quiz name, active/archived.
* Filter by language
    * React-select field, populated from database. This is different from UI language and filter content by content language field.
    * By default equals UI language
    * Includes option 'All' - user can see quizzes with any language
  
### Table rows
Quiz name on left and action buttons on right. Action buttons are

* Copy
    * Duplicate a quiz
* Edit
    * Modify existing quiz -> Open quiz modal
* Archive/Restore
    * Archive/Restore quiz if you are allowed to, with confirm dialog
    * In case archiving quiz is attached to courses, it will be still available however grayed in course structure and labeled with "Archived"
    * App and email (depends on user settings) notification with text "[contentName] in course [courseName] you have created has been archived by [authorName]. [contentName] is still available in course, but you should review its content" is sent to quiz owner in case archiving person, quiz owner and course owner are different users

Table has pagination.

### Ordering
Tasks table is ordered by default quiz name DESC. Clicking on the Name -columns header will toggle sorting by that column DESC<->ASC.

### Creating a quiz
* Click on the + icon on top right of the table header, quiz modal opens.

### Creating a quiz
* Click on the + icon on top right of the table header, modal opens.

***

# Quiz Modal

Save quiz by pressing bottom left Save icon button on the modal or discard from top right.

## Add Quiz (tab in modal)

**Fields**

* Name
    * (required, text input field)
* Description
    * (long text input field, max 1000 characters)
* Private
    * (required, select box)
    * Yes
        * _Only visible for the person creating the quiz_
    * No
        * _Visible for the whole main group_
* Allow others to view or copy questions from this quiz
    * (checkbox, checked by default)
    * _If not checked, users are not allowed to view/copy questions, just use this quiz in exams_
* Show end report
    * (checkbox, checked by default)
    * _Quiz takers can view an end report of points obtained from quiz/each question_
* Pass %
    * (number input field 0-100, no decimals, 80 by default)
    *  _Percentage from points user needs to obtain to pass quiz_
* Show only specific number of questions
    * (checkbox, if checked require fields "Questions simultaneously" and "in percent" below)
        * Questions simultaneously
            * (number input field, required if visible)
            * _Number of questions used in quiz randomly chosen from questions_
        * in percent
            * (checkbox)
            * _Questions simultaneously will mean percentage of questions_
* Skhole certified quiz
    * (checkbox)
    * _Can only be viewed/modified by admin role. Pass percentage cannot be changed in exam. A certificate will be issued from exam where this quiz is used._
* Select Associated Courses...
    * (react-select)
    * _Choose Quiz's course relation. So far, this is not used anywhere except filters in Quizzes section._ 
* Select Associated Lessons...
    * (react-select)
    * _Choose Quiz's lesson relation. So far, this is not used anywhere except filters in Quizzes section._ 
* Show restart button in the end
    * (checkbox)
    * User can restart the quiz when finished. If in exam and no more retries left, user doesn't see restart button.
* Display questions in random order
    * (checkbox)
* Display answers in random order
    * (checkbox)
* Show correct answers
    * (applied only for quiz, checkbox) 
    * defines showing was answer correct or wasn't during quiz completion
* Time limit
    * (number input field, no decimals, default to 3600)
    * _show word "seconds" below this field_
* Results text
    * (long text input field, max 1000 characters)
    * _Show this text after quiz finished_
    * _Show this text on exam certificates_
* Quiz mode
    * (required, select box)
    * Exam
        * _Don't show if user answered correctly when executing the quiz_
        * _Exam tasks can be created based only on this type of quizzes_
    * Quiz
        * _Show if user answered correctly in quiz header when executing the quiz_
        * _This type of quizzes is used to add self-checking quizzes in courses_
* Language
    * [React-select]
    * allowed one option
  
## Questions (tab in modal)

There are 2 panels on this tab. Left side for all existing questions in Skhole and right side for questions currently in selected quiz. After panels there is "Add new question button" on the left side, which will open a new question creation modal over current modal.

### Filters
Possible to filter by [All Questions, Questions from Skhole, Questions from other users, My Questions] (select), related course (react-select), related lesson (react-select), name (text field search on question name, after editing/creation of new quiz/question should be fulfilled with previous value).
* Filter by language
    * React-select field, populated from database. This is different from UI language and filter content by content language field.
    * By default equals UI language
    * Includes option 'All' - user can see questions with any language

Filters will be enabled for left-side questions panel.

### Left side panel
On each row there is name of the question and on right side modify icon and right pointing arrow. Modify icon will open question editing modal over the current modal. Arrow will move the question to the right panel (into current quiz's questions)

### Right side panel
Panel looks same than right side panel except arrow is replaced by trash icon. Clicking that trash icon "Delete question" moves question back from right side panel to left side panel. On the left side of the name, there is also a "sort" icon which can be used to drag and drop sort the questions into preferred order.
