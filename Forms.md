Forms can be found from Group menu in header, navigate to Forms.

Forms are used for creation Documents based on forms.
Forms can be considered as a template for future Document creation.

Groupleader/groupmanager/admin and users with selected sub role Form Editor can access Forms section which can be found under Group menu in header.
Mentioned roles can do next actions based on their access:
* create new Forms
* edit their own Forms
* view/duplicate Forms.
* archive/restore Forms
***

# Forms (section)

### Filters
Possible to filter by Organizations(for WB groupleaders and groupmanagers or admins), text filter on forms name, active/archived.

### Table rows
Forms name on left and action buttons on right. Action buttons are

* Copy
    * Duplicate a form
* Edit
    * Modify existing form -> Open form modal
* Archive/Restore
    * Archive/Restore form if you are allowed to, with confirm dialog

Table has pagination.

### Ordering
Tasks table is ordered by default form name DESC. Clicking on the Name -columns header will toggle sorting by that column DESC<->ASC.

### Creating a form
* Click on the Create form button on top right of the table header, form modal opens.

***

# Form Modal

Save form by pressing bottom left Save icon button on the modal or discard from top right.

**Fields**

* Title
    * (required, text input field)
* Subtitles
  * Field array
      * (long text input field, max 1600 characters)
        * _The text from this text field is static text that will be displayed on document_
      * Remove subtitle button(Trash icon)
* Add new subtitle button
* Image uploader component - allows to upload images in png/jpg/jpeg formats, will be displayed on the left top corner of document.
* Type of field
  * Select with options
    * Checkbox with text description [default]
    * Textarea
    * Subheading
    * Fill in blank
    * Date
* Add new form field
  * Button after clicking adds new field based on selected Type of field
* Form fields
  * Checkbox with text description
    * _Textfield, text from it will be used as a label for checkbox in Document_
  * Textarea
    * _Multiline textfield - will be displayed as static text in Document_
  * Subheading
      * _Textfield, text from it will be used as a label for texfield in Document, just some text on Document in the right position defined by form and filled by teacher during document creation_
  * Fill in blank
    * _Textfield, text from it will be used as a starting label in blank(label) line that will be filled by teacher during document creation_
  * Date
    * _Textfield, text from it will be used as a label for date that will be selected by teacher during document creation_
* Form is ready, publish.
    * (checkbox, checked by default)
    * _If not checked, users are not allowed to copy and use Form in the task_