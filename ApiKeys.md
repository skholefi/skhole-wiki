Groupleaders of organizations with enabled access to Skhole API via Api-kes can add/edit/archive API-keys via Group menu->Groups & Registration codes -> Api keys tab.

Page structure:
* Search api key field
  * (react-select) autocomplete field
  * active/archived
    * radio group
  * archive selected api keys
    * button
        * confirmation modal
        * yes
        * no
  * add new api key
    * button
      * Create Api key modal
        * api key name
          * textfield
        * Api key has been created modal
          * generated api key
            * text displayed only once
            * I saved api kee, close dialog [Button]
  * Table with api keys
    * name
    * key prefix
    * created
    * edit action button
    * paging