There is no UI for this feature, all settings should be added by developers(manually in database).
# Trial Whitelabeled child organization
Share the same logic as usual Trial organization. Applied only for whitelabele child organizations.
* Trial organizations can have
  * own duration of trial period
  * own trial expiration page for each language
  * own organization sign up code

* Developers note
  * In order to allow this feature for any whitelabeled organization:
    * Create a child whitelabeled organization that will be used as a trial org.
    * Edit this organization manually in db and add/edit the next props:
      * trial: true
      * trialDaysDuration: number - the amount of days, how many days the user will be active in trial org
      * trialExpiredPageContent       field which contains expiration page content for each lang. draftJsField - structure of draft.js fields, in order to add we can use question description, save creation of the question and then copy and paste content in db.:
        * 'en-US': draftJsField,
        * 'sv-SE': draftJsField,
        * 'fi-FI': draftJsField,

# Certificates
Any trial organization's users are not able to receive any type of the certificates.
