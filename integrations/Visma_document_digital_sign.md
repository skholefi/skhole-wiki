# Visma digital sign for documents

Integration is designed for adding digital sign to the documents(documents are added based on forms by teachers).
Core logic:
* When organization have turned on documents and forms feature, teachers and higher roles have possibility to request some person(up to 3 persons) to sign document.
* For this they should add signer emails, and request to sign certificate will be sent to signers.
* After request to sign, certificate gets 'pending sign' status, and it's impossible to cancel or change this status.
* If document is not signed during 90 days, it becomes cancelled, and it's possible to add sign again.
* Appearing singed document in Skhole is not immediate process, daily cron job syncs signed documents(or it can be triggered manually in admin tools).
* After successful sync of signed documents, document is displayed as signed with all information.

Visma API documentation - https://sign.visma.net/api/docs/v1/

### Some additional information from task specification:
1. Create the Document
   Groupleader/teacher/orgmanager creates a Document based on some Form, also user can attach up to 3 pdf files with limit 10 MB, that will be included into document and select certificates attached to this document
- After clicking to the save button the document is issued for the user.

2. document contains next information(technically this is one pdf file):
- filled form
- attached certificates
- attached pdf files

2. There is a "Visma sign" button on document page. (document is visible for student and  teacher/admin/manager see Visma sign button)

3. When Groupleader clicks on it he sees the modal "Add email of the persons that will receive a request to sign certificate" Here should be a notification that it is very important to write the correct email address.

4. Groupleader adds email and submits the form(there is email templates that Visma sign service will automatically sent https://sign.visma.net/api/docs/v1/?_ga=2.20525944.1195215798.1673523275-1495413847.1673523275#action-create-invitations-with-templates )

5. Until the (signers) of the certificate does not sign it,  document will have pending sign status ( show in status)

6. When the document is signed by the signer, the groupleader that sent it to be signed should get an email notification about signing

7. Signed document page should contain information about who signed it and the date of signing, a link to the signed pdf document, and a list of certificates links that are included in the Document

8. Sync between Visma documents and Skhole website should be done daily
9. Admin in admin tools should be able to sync statuses at any time by clicking on the button 'Sync Visma sign documents statutses'

## Info for testing and credentials to test account

Username: 33504johanna.honkela@skhole.fi
Password: 4af31a27-10fc-44af-9b9f-ff410de6acca
during signing as a auth method choose "Test identification" url should be identification.frakt.io/