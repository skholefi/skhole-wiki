Header is present on all pages except single lesson pages and student view of quiz pages

***

### (For all users)

# Search field
Possible to search courses and lessons in Skhole. First in view is section for courses, after that for lessons. If nothing is found for section, its header will be hidden. Section header has info on amount of search results found, for example "Lessons (21 results)".

If user types course name "Naistentaudit" (which is an exact hit)

Search results Lessons listing should include all lessons of that course because course name was an exact hit.

If user is logged in, he will find also courses that are private for him/his group.

If user is in [root of the app (/)](https://github.com/elifTech/skhole-ui/wiki/Index-(Courses-listing)) , search results are reflected in the [courses listing (/)](https://github.com/elifTech/skhole-ui/wiki/Index-(Courses-listing)). If user is in any other page, dropdown of results will appear.

Each search try is saved to database for further analyze, with each search session search phrase, user details and search  result are saved.

# Search logic
Search is performed using Mongo query, finding provided text via regular expressions from course/lesson name and single lessons contents. For example if user types "Farmakologia", courses with name "Farmakologia" will be shown in results and all the lessons where word "Farmakologia" is mentioned. Order of results should be so that result hit in name of content is highest rated with exact hit being the most highest. After that content matches. Lessons that are only in private (or private for group) courses will be omitted from results.

# Search - Single lesson search results
Hovering single lesson cards shows the duration of the lesson in minutes. If user does not have subscription to access course where lesson is used, clicking lesson will result to "Want to view this content? Get Skhole for your
organization now!" -modal. If user is allowed to access the lesson, single lesson search results will link to lessons not in scope of any course, for example https://app.skhole.fi/lessons/psychiatric-medication

# Language
Will switch the language of the UI & content.

***

### (For non-logged-in users)

# Signup/Login
* User can click Login to login via email/password, Facebook, Google, Haka or Suomisport account.
* Using other methods than email /password requires that user has a Skhole account set up with the same email address
* Our app might return error "There is no email address associated with your Facebook account so you'll need to choose another login method." in case fb account is linked just by phone without email.
* If user has forgotten his password, he can also reset it from a link here. User needs to provide an email address, where password reset -link is sent. That link needs to be clicked in order for user to provide new password.
* Login form also has Need help? button which opens Skhole's Intercom Articles -based support portal.
* After user is registered with email/pass method, he will be logged in automatically, we don't have this feature for other login methods yet.
* For whitelabeled organization color of text in login button can be customized with secondaryContrastColor in whitelabelingOptions
* For registration sign up code is required, each organization and their subgroups have student and teacher sign up codes
* For whitelableed organizations with enabled memberhship verification, valid unique membership id is required instead of sign up code. 
* If during signup user try to use email that already exists in other organization, user will be proposed to 'Add account to the different organization', user will need to confirm joining via email

***

# (For logged-in users, that are in multiple organization)
Right after login, if user is in multiple organizations, user will see Choose organization modal, with list of all organizations that user is in different statuses[archived, invited, active]  

### Choose organization Modal
  * List with items that represents each organization that user is in  
Items can be active or disabled - depends on status of users  
    When the user signs in but has not confirmed account in new organization yet, he will see "Choose organization modal" but this new organization will be disabled with a notice "To sign in to this organization you should confirm it via email, please check your emailbox"
    * Organization icon(if usual skhole org - default skhole icon)
    * Org name
    * User status in organization
  * Form submit button
***

### (For logged-in users )

# My Group (trial users can see "n days of trial left" without submenu instead)
* Users (teacher, groupleader, groupmanager and admin can see)
* Groups & Registration codes (teacher, groupmanager and groupleader admin can see)
* Organizations (admin can see)
* Tasks
* Statistics
* Classroom
* Admin Tools (admin-only)
* Organization info (teacher, groupmanager and groupleader can see)
* Tags (admin-only)
* Authors (admin and editor)
### Trial users will see Subscribe now button under My group

# Help (icon)
If page does not have tours, link button to Skhole's customer service site (https://support.skhole.fi/faq) or (https://support.skhole.fi/ukk).

Otherwise,

* General help
    * _(link to Skhole's customer service site)_
* Current page help
    * _(Show current page's Tours)_
* Varaa käyttökoulutus(only teacher on finnish ui)
    * Leads to https://www.calendly.com/skhole

# Notifications
Base for [notifications](https://github.com/elifTech/skhole-ui/wiki/Notifications). User an click to open a notification with link, mark notifications as read or click to show older notifications.

# Profile menu
User can get to 
* Profile
* My statistics
* Organization workspaces (visible only for  users that are in  multiple organizations)
* Toggle language (UI/content)
* Log out of the site.
