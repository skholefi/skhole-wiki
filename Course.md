There are two types of courses: normal courses and broadcast courses.

# Course
Course is set of lessons and quizzes. Once user completes all parts he should get certificate.

# Broadcast
Live-stream is basically 1 video embed from YouTube or Vimeo, that is displayed when broadcast time is on. Layout according to
https://projects.invisionapp.com/d/main#/console/20472608/429902995/preview
and
https://projects.invisionapp.com/d/main#/console/20472608/430863682/preview
Time of user on live-stream (during its broadcast time) , should be counted and shown in total on course statistics page on "Completed parts" column in hours, for example 00:50:17 = 50 minutes 17 seconds.

# Podcast
Podcast can have one more many audio files. Audio files are played back right on the subpage. Only playback of files and "lesson" is marked complete when audio played 90% (like currently).
Layout according to
https://projects.invisionapp.com/d/main#/console/20472608/429902994/preview
and
https://projects.invisionapp.com/d/main#/console/20472608/430863681/preview

## "Course details page"

* Course name
* Media durations: X hour(s) XX minutes (hours are omitted if it's zero)
* Recommended study time: X hour(s) XX minutes
    * hours are omitted if it's zero
    * hidden if "Study time estimation type" is "Hide"
* Start Course
    * (button, exists if course not started or user is not logged in)
    * Starts course and route to first item in course hierarchy if user is logged in
    * Opens "Buy Now -modal" in case user is not logged in
* Continue Course
    * (button, exists if course started but not completed)
    * Continues course from next non-completed item in course hierarchy
* Reset course (if there is any progress)
    * Resets course progress
* Give feedback regarding this course
    * Opens Intercom messenger where user can type his message about the course (need to be logged id)
* "Add/Remove to favorites" toggler with heart icon (need to be logged id)
* "Download for offline / Remove from offline" button to download text content of the course for offline access for a month
* Tabs
    * Content
        * Course hierarchy meaning section headers, lessons and quizzes in the specified order
            * Clicking row will open route to the item
            * Item type icons (lesson(play icon when there is video/audio and book icon when only text), quiz)
            * Lesson duration (minutes XX:XX)
            * White circle is displayed in case user is logged in and hasn't completed lesson/quiz yet
            * Blue circle is displayed in case user is logged in and has already completed lesson/quiz
    * Discussions
        * See complete description in article Discussion
    * Notes (hidden for logged in users in case it's empty)
        * Current users course notes by by lessons (lesson name is header)
            * Full highlighted note from lesson is visible
            * Clicking a note links to timeline in video
            * Remove note icon is on right side of the note
            * User can drag and drop notes to their preferred order
    * Authors
        * Each lesson might have authors. All authors of lessons related to the course will be displayed here

After each load of course page GA collects special event course_view, that saves also additional info about course, user and organization.
This allows to get statistics and report directly from Google analytics(Explore -> course_view -> export to spreadsheet). The retention period of this data is 1 year.
***

### "Buy Now -modal"

Clicking routes where user is not allowed to based on his subscription, will result to modal with text
"Want to view this content? Get Skhole for your organization now!" and button "Visit skhole.fi" which will link to www.skhole.fi.

Non-logged users are not allowed in

* Course hierarchy items (lessons, quizzes)
* Discussions
* Notes
* Start course

***

## Pre-course skill evaluation modal

Used to save users estimate of his skill level when starting the course.

The modal will pop up when user starting a new course asking about the current skill level in the topic. Users can give rating to his/her skill level from 1 (Awful) to 5 (Excellent).

This modal will not pop up for users with role admin.

***

## Course Review modal

Used to save user's rating and feedback for the course.

 * Link to Course Review modal shows in lesson header when user completed at least 1 lesson/quiz of the course.
 * Shows in Congratulations Modal when user completed the course.
 * Estimate your skill level on course topic(slider that shows what skills level user got after course completion)

User may skip this modal.

Course review is posted to Slack channel #content-reviews in real time.

***

## "Broadcast details page"

## Broadcast coming
* Countdown to broadcast in {days} {hours}:{minutes}:{seconds} format
* Broadcast name
* Time frame or "Live broadcast" if it's broadcasting now
* Description
* Give feedback regarding this course


## Broadcasting now or passed

* Video player for youtube or vimeo stream
  * when authorized user is watching video, we collect watched time statistics and show it on course statistics
* Broadcast name
* Time frame or "Live broadcast" if it's broadcasting now
* Description
* View broadcast chat (if link to chat is provided)
  * [Desktop] - Opens sidebar near video with chat
  * [Mobile] - Opens chat in the new tab. Button is visible only for vimeo on mobile, for youtube button is hidden because youtube does not allow to watch chat on mobile  
* Give feedback regarding this course


## "Podcast details page"

* The same as normal course but there are no links to lessons. Once user clicks on the lesson, it's expanded, content is shown and audio starts playing.
* When audio is finished and if there is next lesson, after some timeout it will start playing or just expand if there is no audio
* When all podcast lessons are finished user will get certificate if course option "Assign certificate" is checked

***


### Floating icons for admin / user with editor privileges

On bottom left corner there is trash icon and edit icon.

Edit

* Course opens in edit mode (see section Add/Edit Course below)

Trash
* Are you sure you want to delete {courseName}? All related data will be deleted also. 
    * Buttons
        * Oops! no thanks
            * cancel
        * Delete
            * course gets deleted

Copy course - only admins can see this button
* Are you sure you want to copy course?
  * Buttons 
    * copy
    * Oops! no thanks

***

## Add/Edit Course

* Name [required]
    * (text input)
* Slug [required][unique]
    * (text input, will be automatically created on name initialization to format "course-name-like-this")
* Type (only in creation mode)
    * Course
    * Live broadcast
    * Podcast
* Description
    * (textarea)
* Text on the certificate
  * (textarea)
  * max 800 chars
  * text that will be surfaced on certificate of the course completion
* Course image
    * Drag and drop field, where image is automatically shown on saving
* Privacy setting [required]
    * (radio button)
    * Choices
        * Access settings (available only for admin)
          * When selected, see "Configure course access"
        * Private - only owner and skhole admin can see the course
        * Private for an organization (Select groups) -
* Editors who can manage the course
  * (react select field)
  * available only for editors that are content owners or just admins
  * invited editors can manage course, lessons, quizzes and questions attached by its owner
  * If another user is editing course, lesson, quiz or question at same time, we have modal that says "User {firstName} {lastName} is editing lesson at same time. Are you sure you want to take control of editing?"
    Yes / Cancel
    If Yes, then similar modal is presented to the other user.
* Featured (available only for admin)
    * (checkbox)
    * toggles is Course will be promoted as Featured in course listing
* Assign certificate
    * (checkbox)
    * user's that completed all lessons in course will get certificate
* Unauthorized users access (available only for admin) (radio buttons)
    * no access
      * course is not available for not authorized users
    * Listed (default)
      * course is accessible and visible in the list for not authorized uses.
      * if Access settings narrow access down so that authorized user do not have access to such course, user will still see it but won't be able to open lessons/courses from it or assign tasks for the course, also such courses are grayed on the home page courses list.
    * Unlisted
      * the same as listed, but course is not visible in lists, it's just accessible by direct link
    
* Study time estimation type [hidden for broadcast type]
    * Hide (available only for admin)
    * Automatic - shown estimation will be 4* lessons media durations. If no media present in some lesson then that lessons duration should be approximately 100 words/minute. Rounded to the nearest 30 minutes.
    * Manual (2 small number fields) for hour(s) and minutes

* Tags
    * (react-select)
    * hidden for broadcasts
    * add tags to course
    * Any WB editor can use Skhole tags only on its own organization courses, for all other courses, the editor can see assigned Skhole tags but can't remove them or add new ones (on the Skhole tags tooltip "Skhole" must be displayed).
    * Own WB organization tags are hidden for editors from different organizations, except Skhole admins and 'Skhole sisäinen' editors(for them some tooltip should be displayed with the WB org name on course tags edit).
    * If the category of WB organization is non-healthcare - do not display Skhole tags at all, only WB's tags.

* Content categories
    * (react-select)
    * add content categories to course/podcast/livestream

* Language
    * [React-select]
    * allowed one option

* Authors
    * (react-select)
    * hidden for broadcasts
    * add authors to course(different from lessons)

* Forced Progression
  * (checkbox), default false
  * when true, the user studying the course has to advance the course in the fixed order of the elements. Disabled items are grey-out and accessible only after completion of previous item

* Tabs
    * Content (for course type)
        * Course hierarchy meaning section headers, lessons and quizzes in the specified order
            * Each row (section header, lesson, quizzes) has
                * Lesson name, duration and whether lesson is globally free
            * In case section header, can click to edit section header name
            * Draggable icon
                * Drag n drop to reorder
            * Edit button
                * Opens modal to edit lesson/quiz
            * Remove lesson button
                * removes from hierarchy
            * In end of each section there are 2 "buttons" in last row to open modals
                * Add lesson
                * Add quiz
    * Content (for broadcast type)
            * Broadcast url field [required] (accepts youtube or vimeo accessible video urls)
            * Enable chat checkbox (enable chat for video)
            * Broadcast start [required]
            * Broadcast end [required]
    * Content (for normal type)
            * The same as for normal but quizzes are not allowed
* Floating Save-icon

### Configure course access

* When access settings type is selected, then it will be possible to define different permissions' matrix for users for different organizations.
* If whitelabeled organization is selected in Access settings, by default its all users and users in child organization can access the course.  
* When "All normal Skhole organizations" is selected, course owner is changed to admin or whitelabled organization admin depending on the context. Course becomes available for the whole Skhole or Whitelabeled organization depending on the scope. Course, related lessons, quizzes and questions are not editable after course becomes "All normal Skhole organizations". Access can be narrowed down with further described options.
* When 'By roles' is selected(available only for admins), course becomes to be available for all users in Skhole with selected role.
* Skhole admin / Teacher / Org. admin user with editor flag and access to the course can click on "Configure course access" button.
* When no access records are defined, then all organization has access to the course.
* When "Add row" is pressed a new access record row can be added (button scrolls blocks to a new record also)
* Each record can grant access for optionally selected period to all users of organization, specific roles including editor flag, particular users or subgroups
* There are maximum 10 access records for one organization in one course.
* If form is invalid then close/save buttons will be disabled
* Save and close buttons do the same, so permissions are saved only when the course itself is saved
* When "Show teaser to all organization users" checkbox is selected all users that is not specified in access rules see teaser of the course and see the restricted access modal when open such courses.
* Trial organization is not in 'All normal Skhole organizations' access rule. It acts as whitelabeled organization here, in order to provide access to the course, Trial org should be selected in access rules as a separate access record.

### Access record fields 

* Name
    * [required]
    * maximum 50 chars
* Selection type
    * [required]
    * One of: groups, users, roles
    * "Groups" is selected by default
    * When "groups" is selected, then 1-10 groups from organization can be selected to grant access to the course
    * When "users" is selected, then 1-10 users from organization can be selected to grant access to the course
    * When "roles" is selected, then these several options can be selected: organization admin, teachers, students, editors and groupmanagers
* Start date
    * [optional]
    * Define start date when the rule will be applied
* End date
    * [optional]
    * Define end date after what the rule won't be applied
* Actions
    * Delete


***

## Add lesson modal

* Filter by name
    * (text search filter)
* Filter by course (xx items)
    * (filter based on lessons course -relation)
    * Possible to select (Not in any course)
* Filter by language
  * React-select field, populated from database. This is different from UI language and filter content by content language field.
  * By default equals UI language
  * Includes option 'All' - user can see lessons with any language

* Add new -button
    * Opens [Add new lesson modal](https://github.com/elifTech/skhole-ui/wiki/Lesson).
* Items listing
    * Name
    * Created
        * can be sorted by this field, click to toggle asc/desc
        * on hovering, show tooltip of creator {firstName lastName}
    * Action buttons
        * Edit
        * Remove
        * (button is greyed and disabled if user doesn't have access to the functionality)
* Paging
* Done -button
    * (button is greyed and disabled if nothing is selected)

User can choose one or many lessons at same time, then press Save button in bottom right of modal to insert lessons into course hierarchy.

***

## Add quiz modal

* Filter by name
    * (text search filter)
* Filter by quizzes source "Select quizzes"
    * (filter based on creator of course)
        * All Quizzes
        * Quizzes from Skhole
        * Quizzes from other users
        * My Quizzes
* Filter by language
    * React-select field, populated from database. This is different from UI language and filter content by content language field.
    * By default equals UI language
    * Includes option 'All' - user can see quizzes with any language

* Add new -button
    * Opens [Add new quiz modal](https://github.com/elifTech/skhole-ui/wiki/Quizzes).
* Items listing
    * Name
    * Created
        * can be sorted by this field, click to toggle asc/desc
        * on hovering, show tooltip of creator {firstName lastName}
    * Action buttons
        * Edit
        * Remove
        * (button is greyed and disabled if user doesn't have access to the functionality)
* Paging
* Done -button
    * (button is greyed and disabled if nothing is selected)

User can choose one or many quizzes at same time, then press Save button in bottom right of modal to insert quizzes into course hierarchy.

***

## Add lesson content modal

* Name
    * (text input)
* Path name
    * (text input, will be automatically created on name initialization to format "lesson-name-like-this")
* Embed video from YouTube or Vimeo
    * (text input)
* Upload media
    * (button)
    * Opens browser file selection dialog, uploads file directly to s3 bucket showing progress and estimated left time to upload file
    * Selected file should be less than 1000Mb and 1 hour duration
    * If uploaded file resolution is less than 360p, then it will be stretched to 360p
    * If original video resolution is smaller than 720p/1080p then these formats would be just skipped instead of their stretching 
    * If uploaded file contains only audio stream then only ogg and aac files will be generated
    * If uploaded file contains only video stream then only preview image and video files without audio files will be generated 
    * Different codes are used selected for Presentation and Normal videos modes that impact on video quality and size
    * Transcoding is started once lesson is saved in create mode and right after file selection and uploading in edit mode
    * When transcoding is started "Show variants" is expanded and transcoding progress with estimate  of left time for current processing job are shown.
    * Transcoding server process only one job of one user at the same time on the server, if it's not able to process job within 30 minutes it will be canceled and error would be shown
    * Priorities of jobs by desc: image preview, 360p, aac, ogg, 720p, 1080p
    * If user opens lesson without lesson but with transcoding jobs we show "video is still being transcoded, try again soon" in place of video. 
    * If upload video, then user is notified (also via email) when lowest quality (360p) video is ready to be viewed in player.
    * If upload audio only, then user is notified when audio is ready to be listened in all audio formats.
    * We also log time it took to upload file, time it took to process, original file duration and resolution (more transcoding statistics can be find out in https://app.asana.com/0/909953222341876/1189470743764273)
    * If uploading is still in progress and user try to close/save lesson the confirmation modal will be shown that user will lose the content that is uploading
* Cancel upload
    * Shown when file to transcode is uploading
    * When it's clicked uploading will be aborted
* Cancel media jobs
   * Shown when file to transcode is uploaded, lesson is saved and transcoding jobs are started
   * Cancels all jobs of current user for this lesson with pending/processing statuses 
* Show variants
    * Toggle visibility of different media variants
        * Thumbnail
        * 1080p
        * 720p
        * 360p
        * m4a
        * ogg
* Sources
    * (DraftJS)
    * keeps formatting on copypaste, supports images and links
    * images which user uploads into lesson content are resized to maximum width of 1400px (height scaled relatively automatically) and compressed.
* Show attachments
    * Toggle visibility of different media variants
        * Listing of files attached into lesson
        * Add attachment -link
            * Opens browser file selection dialog, uploads file to S3 bucket and reference/link to file into to lesson
* Globally free lesson
    * Makes lesson free content to everyone (even non-logged-in users)
* Select authors
    * (react-select)
* Content
    * (DraftJS)
    * keeps formatting on copypaste, supports images and links
* Language
    * [React-select]
    * allowed one option