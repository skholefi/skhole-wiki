Notifications are displayed in [Header](https://github.com/elifTech/skhole-ui/wiki/Header)

***

## Types of notifications

### Task - Exam

Text

* "You have been assigned an exam:<br /><b>{name}</b><br />Completion time: <b>{from}</b> - <b>{to}</b>"
Action
* Opens exam (user still needs to click on the Start Exam button)

### Task - Course

Text

* "You have been assigned a task<br />Complete course: <b>{name}</b><br />Completion time: <b>{from}</b> - <b>{to}</b>"

Action

* Opens courses page (user still needs to click "Start course" or click open some lesson to start the course)

### Task - Message

Text

* "You have a new message from <b>{firstName}</b> <b>{lastName}</b>" + truncated message...
Action
* Opens up the full message in a scrollable/closable modal window.

### Task - Assignment

Text

* You have been assigned an open assignment task<br /><b>{name}</b><br />Completion time: <b>{from}</b> - <b>{to}</b>
* Opens list of tasks


### Tasks - All users completed exam task</s> (temporary disabled)

Text

* "All users completed task <b>{name}</b><br />Completion time: <b>{from}</b> - <b>{to}</b>"

### Tasks - All users completed course task

Text

* "All users completed task <b>{course}</b><br /><b>{courseName}</b>"

### Tasks - All users completed message task

Text

* "All users completed task <b>Message from {user}</b><br />{truncatedText}</b>"

### Excel export

Text

* "Excel export of {type} now available.<br />Download here ({size})"

Action

* Excel file download starts

### Discussions - New reply to followed discussion

Text

* "<strong>{lastName firstName}</strong><br />replied to a post you follow"

Action

* Route to topic

_### User shared his notes with teachers/whole group_ (need to check???)

_Text_

_* "<strong>{lastName firstName}</strong><br /> shared {courseName} notes with you"_

_Action_

_* Open public profile of user_

### Certificate is generated (only email notification)

Subject

* Certificate of course {courseName}
* Certificate of exam {quizName}

Text

#### Course
* Congratulations, {courseName} completed!
* View certificate
* certificateURL = link

#### Exam
* Congratulations, you have passed {quizName}!
* View certificate
* certificateURL = link


### Also

* _Once task has been prolonged new notification will be send to assigners_
* _Notifications about tasks that are not available (archived task or user changed organization/subgroup) user will be hidden_
* Task notifications are sending by cron with some delay, while all other notifications are send without any delay.
* Notification can be marked as read automatically in case its due date is passed.

***

## Read all notifications

* When "Mark all as read" button is pressed, all notifications becomes read, so badge near "Ring" icon will disappear
and list of notifications will contain only 'No new notifications'

## Show older
By default only new notifications are shown. By clicking this button the full list of notifications is shown.

